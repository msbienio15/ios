import XCTest
import RealmSwift
import RxSwift
import RxBlocking
import RxTest
@testable import Luca_Auto_Tests

 class RealmDataRepoTests: XCTestCase {

    // Both repos work on the same file, so accessing the same file should always produce errors as the encryption settings are different
    var repo: SomeDatasetRepo!
    var encryptedRepo: SomeDatasetEncryptedRepo!
    let singleInputData = SomeDataset(identifier: 0, someString: "some string", someInt: 1234, someArray: [2345, 456, 235], someOptional: "ih9")
    let multipleInputData = [
        SomeDataset(identifier: 0, someString: "some string", someInt: 1234, someArray: [2345, 456, 235], someOptional: "ih9"),
        SomeDataset(identifier: 1, someString: "some string 123 ", someInt: 213, someArray: [2345, 235], someOptional: nil)
    ]
    var scheduler: TestScheduler!

    override func setUpWithError() throws {
        scheduler = TestScheduler(initialClock: 0)
        repo = SomeDatasetRepo()
        encryptedRepo = SomeDatasetEncryptedRepo()
        _ = try repo.removeFile().toBlocking().toArray()

        continueAfterFailure = false
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_writeAndReadSingleDataSet_loadedDataSetEqualsOrigin() throws {
        try writeAndReadSingleDataSet_loadedDataSetEqualsOrigin(with: repo)
    }

    func test_writeAndReadEncryptedSingleDataSet_loadedDataSetEqualsOrigin() throws {
        try writeAndReadSingleDataSet_loadedDataSetEqualsOrigin(with: encryptedRepo)
    }

    func test_writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin() throws {
        try writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin(with: repo)
    }

    func test_writeAndReadEncryptedMultipleDataSet_loadedDataSetEqualsOrigin() throws {
        try writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin(with: encryptedRepo)
    }

    func test_writeAndRemoveAllData_restoresEmptyArray() throws {
        try writeAndRemoveAllData_restoresEmptyArray(with: repo)
    }

    func test_encrypted_writeAndRemoveAllData_restoresEmptyArray() throws {
        try writeAndRemoveAllData_restoresEmptyArray(with: encryptedRepo)
    }

    func test_writeMultipleDataAndRemoveOneObject_restoresSingleItem() throws {
        try writeMultipleDataAndRemoveOneObject_restoresSingleItem(with: repo)
    }

    func test_encrypted_writeMultipleDataAndRemoveOneObject_restoresSingleItem() throws {
        try writeMultipleDataAndRemoveOneObject_restoresSingleItem(with: encryptedRepo)
    }

    func test_writeNonEncryptedAndLoadWithKey_fails() throws {
        let stream = try repo.store(object: singleInputData)
            .flatMap { _ in self.encryptedRepo.restore() }
            .materializeAndBlock()

        XCTAssertEqual(stream?.count, 1)
        XCTAssert(stream?.first?.error != nil)
    }

    func test_writeEncryptedAndLoadWithNoKey_fails() throws {
        let stream = try encryptedRepo.store(object: singleInputData)
            .flatMap { _ in self.repo.restore() }
            .materializeAndBlock()

        XCTAssertEqual(stream?.count, 1)
        XCTAssert(stream?.first?.error != nil)
    }

    func test_changeNonEncryptedToEncrypted_readEncrypted() throws {
        let stream = try repo.store(object: singleInputData)
            .flatMapCompletable { _ in self.repo.changeEncryptionSettings(oldKey: nil, newKey: encryptionKey) }
            .andThen(encryptedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([singleInputData]), .completed])
    }

    func test_changeNonEncryptedToEncrypted_failAtReadNonEncrypted() throws {
        let stream = try repo.store(object: singleInputData)
            .flatMapCompletable { _ in self.repo.changeEncryptionSettings(oldKey: nil, newKey: encryptionKey) }
            .andThen(repo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream?.count, 1)
        XCTAssert(stream?.first?.error != nil)
    }

    func test_changeEncryptedToNonEncrypted_read() throws {
        let stream = try encryptedRepo.store(object: singleInputData)
            .flatMapCompletable { _ in self.repo.changeEncryptionSettings(oldKey: encryptionKey, newKey: nil) }
            .andThen(repo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([singleInputData]), .completed])
    }

    func test_changeEncryptedToNonEncrypted_failAtReadEncrypted() throws {
        let stream = try encryptedRepo.store(object: singleInputData)
            .flatMapCompletable { _ in self.repo.changeEncryptionSettings(oldKey: encryptionKey, newKey: nil) }
            .andThen(encryptedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream?.count, 1)
        XCTAssert(stream?.first?.error != nil)
    }

    private func writeAndReadSingleDataSet_loadedDataSetEqualsOrigin(with selectedRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset>) throws {
        let stream = try selectedRepo.store(object: singleInputData)
            .map { $0 == self.singleInputData }
            .filter { $0 }
            .asObservable()
            .flatMap { _ in selectedRepo.restore() }
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([singleInputData]), .completed])
    }

    private func writeAndReadMultipleDataSet_loadedDataSetEqualsOrigin(with selectedRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset>) throws {
        let stream = try selectedRepo.store(objects: multipleInputData)
            .map { $0 == self.multipleInputData }
            .filter { $0 }
            .asObservable()
            .flatMap { _ in selectedRepo.restore() }
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next(multipleInputData), .completed])
    }

    private func writeAndRemoveAllData_restoresEmptyArray(with selectedRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset>) throws {
        let stream = try selectedRepo.store(objects: multipleInputData)
            .flatMapCompletable { _ in selectedRepo.removeAll() }
            .andThen(selectedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([]), .completed])
    }

    private func writeMultipleDataAndRemoveOneObject_restoresSingleItem(with selectedRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset>) throws {
        let stream = try selectedRepo.store(objects: multipleInputData)
            .flatMapCompletable { _ in selectedRepo.remove(identifiers: [0]) }
            .andThen(selectedRepo.restore())
            .materializeAndBlock()

        XCTAssertEqual(stream, [.next([multipleInputData[1]]), .completed])
    }

 }

 struct SomeDataset: DataRepoModel, Equatable {

    var identifier: Int?

    var someString: String
    var someInt: Int
    var someArray: [Int]
    var someOptional: String?

 }

 class SomeDatasetRealmModel: RealmSaveModel<SomeDataset> {

    @objc dynamic var someString = ""
    @objc dynamic var someInt = 0
    var someArray = List<Int>()

    @objc dynamic var someOptional: String?

    override func create() -> SomeDataset {
        return SomeDataset(identifier: nil, someString: "", someInt: 0, someArray: [], someOptional: nil)
    }

    override func populate(from: SomeDataset) {
        super.populate(from: from)
        someString = from.someString
        someInt = from.someInt
        someArray.removeAll()
        someArray.append(objectsIn: from.someArray)
        someOptional = from.someOptional
    }

    override var model: SomeDataset {
        var m = super.model
        m.someArray = Array(someArray)
        m.someString = someString
        m.someInt = someInt
        m.someOptional = someOptional
        return m
    }
 }

 class SomeDatasetRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset> {

    override func createSaveModel() -> SomeDatasetRealmModel {
        return SomeDatasetRealmModel()
    }

    init() {
        super.init(filenameSalt: "TestRepo", schemaVersion: 0)
    }
 }

 let encryptionKey = Data(hex: "a0f9ca456622c28dba9827d75f78de541dd05e4a807770bbe2cf8dedd307d45d3b754e78ebc5345efacfaa6ed815c4930bf4f8a9ed8069ff88956b6696e4767a")
 class SomeDatasetEncryptedRepo: RealmDataRepo<SomeDatasetRealmModel, SomeDataset> {

    override func createSaveModel() -> SomeDatasetRealmModel {
        return SomeDatasetRealmModel()
    }

    init() {
        super.init(filenameSalt: "TestRepo", schemaVersion: 0, encryptionKey: encryptionKey)
    }
 }
