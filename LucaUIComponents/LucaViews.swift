import Foundation
import UIKit

@IBDesignable
public class LightStandardButton: RoundedLucaButton {}

@IBDesignable
public class DarkStandardButton: RoundedLucaButton {
	internal override func borderWidth() -> CGFloat {return 1}
}

public class SelfSizingLabel: UILabel {

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setup()
	}

	private func setup() {
		self.adjustsFontForContentSizeCategory = true
	}
}

public class Luca14PtLabel: SelfSizingLabel {}
public class Luca14PtBlackLabel: SelfSizingLabel {}
public class Luca14PtBoldLabel: SelfSizingLabel {}
public class Luca16PtLabel: SelfSizingLabel {}
public class Luca20PtBoldLabel: SelfSizingLabel {}
public class TANLabel: SelfSizingLabel {}
public class Luca16PtBoldLabel: SelfSizingLabel {}
public class Luca60PtLabel: SelfSizingLabel {}
public class Luca12PtLabel: SelfSizingLabel {}
public class PrivateMeetingTimerLabel: SelfSizingLabel {}

@IBDesignable
public class WhiteButton: RoundedLucaButton {}
