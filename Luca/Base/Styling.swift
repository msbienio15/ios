import Foundation
import UIKit
import LucaUIComponents

internal struct Styling {
	static func applyStyling() {
		// Alerts
		UIView.appearance(whenContainedInInstancesOf: [UIAlertController.self]).tintColor = UIColor.lucaBlue

		// navigation bar
		let attrsTitle = [NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20), NSAttributedString.Key.foregroundColor: UIColor.white]
		UINavigationBar.appearance().titleTextAttributes = attrsTitle
		UINavigationBar.appearance().barTintColor = UIColor.black
		UINavigationBar.appearance().tintColor = UIColor.white
		UINavigationBar.appearance().isTranslucent = false
		UINavigationBar.appearance().shadowImage = UIImage()

		let attrsButton = [
			NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
			NSAttributedString.Key.foregroundColor: UIColor.white
		]
		UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes(attrsButton, for: .normal)
		UIBarButtonItem.appearance(whenContainedInInstancesOf: [UINavigationBar.self]).setTitleTextAttributes(attrsButton, for: .highlighted)

		DarkStandardButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
		DarkStandardButton.appearance().titleLabelColor = UIColor.white
		DarkStandardButton.appearance().backgroundColor = UIColor.clear
		DarkStandardButton.appearance().borderColor = UIColor.white

		LightStandardButton.appearance().titleLabelFont = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
		LightStandardButton.appearance().titleLabelColor = UIColor.black
		LightStandardButton.appearance().backgroundColor = Asset.lucaBlue.color

        LucaDefaultTextField.appearance().backgroundColor = .clear
        LucaDefaultTextField.appearance().font = FontFamily.Montserrat.medium.font(size: 14)
        LucaDefaultTextField.appearance().textColor = UIColor.white
        LucaDefaultTextField.appearance().borderColor = Asset.luca747480.color

        Luca14PtLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 14))
        Luca14PtLabel.appearance().textColor = UIColor.white

        Luca14PtBlackLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 14))
        Luca14PtBlackLabel.appearance().textColor = UIColor.black

		Luca14PtBoldLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 14))
		Luca14PtBoldLabel.appearance().textColor = UIColor.white

		Luca20PtBoldLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 20))
		Luca20PtBoldLabel.appearance().textColor = UIColor.white

		TANLabel.appearance().font = Styling.font(font: UIFont.monospacedDigitSystemFont(
																								ofSize: 20,
																								weight: .bold))
		TANLabel.appearance().textColor = UIColor.white

		Luca16PtLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 16))
		Luca16PtLabel.appearance().textColor = Asset.lucaBlack87Percent.color

        Luca16PtBoldLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.bold.font(size: 16))
        Luca16PtBoldLabel.appearance().textColor = UIColor.white

		Luca60PtLabel.appearance().font = Styling.font(font: UIFont.monospacedDigitSystemFont(
																										ofSize: 60,
																										weight: .semibold))
		Luca60PtLabel.appearance().textColor = Asset.lucaBlack.color

		Luca12PtLabel.appearance().font = Styling.font(font: FontFamily.Montserrat.medium.font(size: 12))
		Luca12PtLabel.appearance().textColor = Asset.lucaBlack.color

		// NewPrivateMeetingViewController

		Luca14PtLabel.appearance(whenContainedInInstancesOf: [ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack87Percent.color
		Luca14PtBoldLabel.appearance(whenContainedInInstancesOf: [ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

        Luca16PtBoldLabel.appearance(whenContainedInInstancesOf: [NewPrivateMeetingViewController.self, ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

		PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [ActiveCheckinNavigationController.self]).font = Styling.font(font: UIFont.monospacedDigitSystemFont(ofSize: 16, weight: .bold))
		PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [NewPrivateMeetingDetailsViewController.self, ActiveCheckinNavigationController.self]).font = Styling.font(font: UIFont.monospacedDigitSystemFont(ofSize: 16, weight: .regular))
		PrivateMeetingTimerLabel.appearance(whenContainedInInstancesOf: [ActiveCheckinNavigationController.self]).textColor = Asset.lucaBlack.color

		// ChildrenListViewController embedded in the CheckinNavigationController (needs to have a light interface)

		WhiteButton.appearance().titleLabelColor = UIColor.black
		WhiteButton.appearance().backgroundColor = .white

		self.styleEmbeddedChildrenViewControllers()
	}

	private static func font(font: UIFont, textStyle: UIFont.TextStyle = UIFont.TextStyle.body, maximumFontSize: CGFloat = 60) -> UIFont {
		let fontMetrics = UIFontMetrics(forTextStyle: textStyle)
		return fontMetrics.scaledFont(for: font, maximumPointSize: maximumFontSize)
	}

	private static func styleEmbeddedChildrenViewControllers() {
		let navBarAppearance = UINavigationBar.appearance(whenContainedInInstancesOf: [
																												ActiveCheckinNavigationController.self])

		let attrsTitle = [
			NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 20),
			NSAttributedString.Key.foregroundColor: UIColor.black]

		navBarAppearance.titleTextAttributes = attrsTitle
		navBarAppearance.barTintColor = Asset.lucaCheckinGradientBottom.color
		navBarAppearance.tintColor = UIColor.black
		navBarAppearance.isTranslucent = false
		navBarAppearance.shadowImage = UIImage()

		let barButtonAppearance = UIBarButtonItem.appearance(whenContainedInInstancesOf: [
																													UINavigationBar.self,
																													ActiveCheckinNavigationController.self])

		let attrsButtonChildren = [
			NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
			NSAttributedString.Key.foregroundColor: UIColor.black
		]

		barButtonAppearance.setTitleTextAttributes(attrsButtonChildren, for: .normal)
		barButtonAppearance.setTitleTextAttributes(attrsButtonChildren, for: .highlighted)

		let luca14ptLabel = Luca14PtLabel.appearance(whenContainedInInstancesOf: [
																												EmbeddedChildrenListViewController.self,
																												ActiveCheckinNavigationController.self])
		luca14ptLabel.textColor = UIColor.lucaBlack
		Luca14PtBoldLabel.appearance(whenContainedInInstancesOf: [
																		EmbeddedChildrenListViewController.self,
																	ActiveCheckinNavigationController.self]).textColor = UIColor.lucaBlack

        Luca16PtLabel.appearance(whenContainedInInstancesOf: [DocumentListViewController.self]).textColor = UIColor.white

        Luca16PtBoldLabel.appearance(whenContainedInInstancesOf: [
                                                                        EmbeddedChildrenListViewController.self,
                                                                    ActiveCheckinNavigationController.self]).textColor = UIColor.lucaBlack

		UITableView.appearance(whenContainedInInstancesOf: [EmbeddedChildrenListViewController.self]).separatorColor = .yellow
	}
}

protocol LucaModalAppearance {
	func applyColors()
}

extension LucaModalAppearance where Self: UIViewController {
	func applyColors() {
		self.navigationController?.navigationBar.barTintColor = Asset.luca1d1d1d.color
		self.view.backgroundColor = Asset.luca1d1d1d.color
		self.navigationController?.navigationBar.shadowImage = UIImage()

		let attrsButton = [
			NSAttributedString.Key.font: FontFamily.Montserrat.bold.font(size: 14),
			NSAttributedString.Key.foregroundColor: Asset.lucaBlue.color
		]

		self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(attrsButton, for: .normal)
		self.navigationItem.rightBarButtonItem?.setTitleTextAttributes(attrsButton, for: .highlighted)
		self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(attrsButton, for: .normal)
		self.navigationItem.leftBarButtonItem?.setTitleTextAttributes(attrsButton, for: .highlighted)
	}
}
