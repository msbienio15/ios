import Foundation

/// Describes an symbolic error that provides no visual cues.
/// e.g. User actively cancels an action but this actions needs to throw an error
protocol SilentErrorType: Error {
}

enum SilentError: SilentErrorType {
    case userDeclined
}
