import Foundation

extension Array where Element: Hashable {

    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.symmetricDifference(otherSet))
    }

}
extension Array {
    func difference(from other: [Element], equalityChecker: (Element, Element) -> Bool) -> [Element] {
        let thisExclusiveOther = self.filter { firstElement in
            !other.contains(where: { secondElement in
                equalityChecker(firstElement, secondElement)
            })
        }
        let otherExclusiveThis = other.filter { firstElement in
            !self.contains(where: { secondElement in
                equalityChecker(firstElement, secondElement)
            })
        }
        return thisExclusiveOther + otherExclusiveThis
    }

    func subtract(_ other: [Element], equalityChecker: (Element, Element) -> Bool) -> [Element] {
        filter { firstElement in
            !other.contains(where: { secondElement in
                equalityChecker(firstElement, secondElement)
            })
        }
    }

}

public extension Array where Element: OptionalType {

    func unwrapOptional() -> [Element.Wrapped] {
        filter { (value) in
            if value.value == nil {
                return false
            }
            return true
        }
        .map { $0.value! }
    }

}
