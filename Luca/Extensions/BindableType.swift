import UIKit
import RxSwift

protocol BindableType {
	associatedtype ViewModelType
	var viewModel: ViewModelType! { get set }
	func bindViewModel()
}

extension BindableType where Self: UIViewController {
	mutating func bindViewModel(to model: Self.ViewModelType) {
		viewModel = model
		loadViewIfNeeded()
		bindViewModel()
	}
}

protocol ViewControllerOwnedVM {
	var owner: UIViewController? {get}
	func rxErrorAlert(for error: Error) -> Completable
}

extension ViewControllerOwnedVM {
	func rxErrorAlert(for error: Error) -> Completable {
		guard let owner = owner else {return Completable.empty()}
		return UIAlertController.infoAlertRx(
					viewController: owner,
					title: L10n.MainTabBarViewController.ScannerFailure.title,
					message: error.localizedDescription)
					.ignoreElementsAsCompletable()
					.andThen(Completable.error(error)) // Push the error through to retry the stream
	}

}

protocol HandlesLucaErrors {
	func processErrorMessages(error: Error, completion:(() -> Void)?)
}

extension HandlesLucaErrors where Self: UIViewController {
	func processErrorMessages(error: Error, completion:(() -> Void)? = nil) {
		if let printableError = error as? PrintableError {
			let alert = UIAlertController.infoAlert(
				title: printableError.title,
				message: printableError.message, onOk: completion)
			self.present(alert, animated: true, completion: nil)
		} else if let localizedError = error as? LocalizedTitledError {
			let alert = UIAlertController.infoAlert(
				title: localizedError.localizedTitle,
				message: localizedError.localizedDescription, onOk: completion)
			self.present(alert, animated: true, completion: nil)
		} else {
			let alert = UIAlertController.infoAlert(
				title: L10n.Navigation.Basic.error,
				message: L10n.General.Failure.Unknown.message(error.localizedDescription), onOk: completion)
			self.present(alert, animated: true, completion: nil)
		}
	}

    func processErrorMessagesRx(viewController: UIViewController, error: Error) -> Completable {
        Completable.create { observer in
            self.processErrorMessages(error: error) {
                observer(.completed)
            }
            return Disposables.create()
        }
    }

}
