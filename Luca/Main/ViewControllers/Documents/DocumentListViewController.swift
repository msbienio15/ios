import UIKit
import RxSwift
import JGProgressHUD
import LucaUIComponents

class DocumentListViewController: UIViewController {

    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var addButton: LightStandardButton!
    @IBOutlet weak var dataAccessBanners: DataAccessesBanners!
	@IBOutlet weak var notificationStackView: UIStackView!

    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var dataAccessHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!

    private var progressHud = JGProgressHUD.lucaLoading()

    weak var timeDifferenceView: TimeDifferenceView?

    private var disposeBag: DisposeBag?

    private var documents = [Document]()
    private var persons = [Person]()
    private var groupedDocuments: [String: [Document]] = [:]
    private var testScannerNavController: UINavigationController?

    private var dataAccessHeight: CGFloat = 84
    private var noDataAccessHeight: CGFloat = 0

    private let calendarURLString = "https://www.luca-app.de/coronatest/search"

    private let revalidationKey = "revalidationKey"

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

		setup()
        setupNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAccessibility()

        installObservers()
		checkTimesync()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        disposeBag = nil
    }
}

// MARK: - Setup

extension DocumentListViewController {

    private func setup() {
        notificationStackView.removeAllArrangedSubviews()
        notificationStackView.isHidden = true

        setupStackView()
        setupApplicationStateObserver()
    }

    private func setupNavigationBar() {
        set(title: L10n.My.Luca.title)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        navigationItem.rightBarButtonItems = [UIBarButtonItem(image: Asset.calendar.image, style: .plain, target: self, action: #selector(calendarTapped)),
                                              UIBarButtonItem(image: Asset.addPersonWhite.image, style: .plain, target: self, action: #selector(addPersonTapped))]
    }

    func setupStackView() {
        stackView.removeAllArrangedSubviews()

        var groupViews: [DocumentGroupView] = []
        groupedDocuments.forEach { (key, docs) in
            let isChildren = persons.filter { $0.formattedName == key }.count > 0
            let itemViews: [DocumentView] = docs.compactMap { DocumentViewFactory.createView(for: $0, isExpanded: false, with: self) }
            let groupView = DocumentGroupView(title: key, isChildren: isChildren, views: itemViews)
            groupViews.append(groupView)
        }

        groupViews
            .sorted { !$0.isChildren && $1.isChildren }
            .forEach { stackView.addArrangedSubview( $0 ) }
    }

    func setupAccessibilityViewsEmptyState() {
        self.view.accessibilityElements = [subtitleLabel, descriptionLabel, addButton].map { $0 as Any }
    }

    func setupAccessibilityViews() {
        self.view.accessibilityElements = [subtitleLabel, descriptionLabel, scrollView, addButton].map { $0 as Any }
    }

    private func installObservers() {
        let newDisposeBag = DisposeBag()
        revalidateIfNeeded()
            .andThen(loadPersons())
            .andThen(loadDocuments())
            .subscribe()
            .disposed(by: newDisposeBag)

        Observable<Void>.merge(
            Observable.just(Void()),
            ServiceContainer.shared.accessTraceIdChecker.onNewData.map { _ in Void() }
        )
        .debounce(.milliseconds(500), scheduler: MainScheduler.instance)
        .observe(on: MainScheduler.instance)
        .flatMap { _ in self.dataAccessBanners.fetch(useOnlyCachedData: true) }
        .subscribe()
        .disposed(by: newDisposeBag)

        dataAccessBanners
            .onShowWarningLevel
            .do(onNext: { warningLevel in
                let vc = ViewControllerFactory.History.createHistoryViewController(filterForWarningLevel: warningLevel)
                self.navigationController?.pushViewController(vc, animated: true)
            })
            .drive()
            .disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

    private func loadDocuments() -> Completable {
        ServiceContainer.shared.documentRepoService
            .currentAndNewTests
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .filter({ newDocuments in
                if self.documents.count != newDocuments.count { return true }
                return !self.documents.elementsEqual(newDocuments) { $0.originalCode == $1.originalCode }
            })
            .observe(on: MainScheduler.instance)
            .do(onNext: { docs in
                self.documents = docs.compactMap { $0 }
                self.groupDocuments()
                self.updateViewControllerStyle()
                self.setupStackView()
            }, onSubscribe: {
                self.updateViewControllerStyle()
            })
            .ignoreElementsAsCompletable()
    }

    private func loadPersons() -> Completable {
        ServiceContainer.shared.personService
            .retrieve()
            .subscribe(on: ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { entries in
                self.persons = entries
            })
            .asCompletable()
    }

    private func groupDocuments() {
        guard let firstName = LucaPreferences.shared.firstName, let lastName = LucaPreferences.shared.lastName else { return }

        groupedDocuments = [:]

        let docs = documentsFor(firstname: firstName, lastname: lastName, includeDocsWithoutUser: true)
        if docs.count > 0 {
            groupedDocuments["\(firstName) \(lastName)"] = docs
        }

        persons.forEach { person in
            let docs = documentsFor(firstname: person.firstName, lastname: person.lastName)
            if docs.count > 0 {
                groupedDocuments[person.formattedName] = docs
            }
        }
    }

    private func documentsFor(firstname: String, lastname: String, includeDocsWithoutUser: Bool = false) -> [Document] {
        return documents.compactMap { document in
            if let doc = document as? AssociableToIdentity {
                if doc.belongsToUser(withFirstName: firstname, lastName: lastname) {
                    return document
                }
            } else if includeDocsWithoutUser {
                return document
            }

            return nil
        }
    }

    private func name(for document: Document) -> String {
        var name = ""
        for (key, items) in groupedDocuments {
            if items.filter({$0.identifier == document.identifier}).count > 0 {
                name = key
            }
        }

        return name
    }

    private func getLastRevalidationDate() -> Single<Date?> {
        return ServiceContainer.shared.keyValueRepo.load(revalidationKey, type: Date?.self)
            .catch { _ in Single.just(nil) }
    }

    private func revalidateIfNeeded() -> Completable {
        return getLastRevalidationDate()
            .flatMapCompletable { date in

                if date == nil || !Calendar.current.isDateInToday(date!) {
                    let sc = ServiceContainer.shared
                    return sc.documentProcessingService.revalidateSavedTests()
                        .andThen(sc.keyValueRepo.store(self.revalidationKey, value: Date()))
                }

                return Completable.empty()
            }
    }

    private func setupApplicationStateObserver() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidEnterBackground(_:)),
            name: UIApplication.didEnterBackgroundNotification,
            object: nil)

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationDidBecomeActive(_:)),
            name: UIApplication.didBecomeActiveNotification,
            object: nil)
    }

    @objc
    func applicationDidEnterBackground(_ notification: NSNotification) {
        self.disposeBag = nil

        let scannerViewController = self.testScannerNavController?.viewControllers.first as? TestQRCodeScannerController
        scannerViewController?.scannerService?.endScanner()
        self.testScannerNavController?.dismiss(animated: true, completion: nil)
    }

    @objc
    func applicationDidBecomeActive(_ notification: NSNotification) {
        self.installObservers()
        self.checkTimesync()
    }

    private func updateViewControllerStyle() {
        DispatchQueue.main.async {
            let isEmptyState = self.documents.isEmpty
            self.emptyStateView.isHidden = !isEmptyState
            self.stackView.isHidden = isEmptyState
            isEmptyState ? self.setupAccessibilityViewsEmptyState() : self.setupAccessibilityViews()
        }
	}
}

// MARK: - Actions

extension DocumentListViewController {

    @IBAction func addTestPressed(_ sender: UIButton) {
        testScannerNavController = ViewControllerFactory.Document.createTestQRScannerViewController()
        if let scanner = testScannerNavController {
            scanner.modalPresentationStyle = .overFullScreen
            scanner.definesPresentationContext = true
            present(scanner, animated: true, completion: nil)
        }
    }

    @objc private func addPersonTapped() {
        let viewController = ViewControllerFactory.Children.createChildrenListViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }

    @objc private func calendarTapped() {
        if let url = URL(string: calendarURLString) {
            UIApplication.shared.open(url)
        }
    }
}

extension DocumentListViewController {
    private func delete(document: Document, viewController: UIViewController) {
        let alert = UIAlertController.yesOrNo(title: L10n.Test.Delete.title, message: L10n.Test.Delete.description, onYes: {
            _ = ServiceContainer.shared.documentProcessingService.remove(document: document)
                .observe(on: MainScheduler.instance)
                .do(onError: { error in
                    let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.error, message: L10n.Test.Result.Delete.error)
                    viewController.present(alert, animated: true, completion: nil)
                }, onCompleted: {
                    self.navigationController?.popViewController(animated: true)
                })
                .subscribe()
        }, onNo: nil)

        viewController.present(alert, animated: true, completion: nil)
    }
}

// MARK: - Timesync

extension DocumentListViewController {
    private func checkTimesync() {
        guard let disposeBag = disposeBag else { return }

        ServiceContainer.shared.backendMiscV3.fetchTimesync()
            .asSingle()
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { time in
                // only show the view, if there is a time difference > 5 min
                let isValid = (time.unix - 5 * 60) ... (time.unix + 5 * 60) ~= Int(Date().timeIntervalSince1970)
                isValid ? self.hideTimeDifferenceView() : self.showTimeDifferenceView()
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func showTimeDifferenceView() {
        if timeDifferenceView == nil {
            timeDifferenceView = TimeDifferenceView.fromNib()
            notificationStackView.addArrangedSubview(timeDifferenceView!)
        }
        notificationStackView.isHidden = false

        timeDifferenceView?.isHidden = false
    }

    private func hideTimeDifferenceView() {
        timeDifferenceView?.isHidden = true
        notificationStackView.isHidden = true
    }
}

// MARK: - DataAccess

extension DocumentListViewController {

    private func showDataAccessAlert(onOk: (() -> Void)? = nil, accesses: [SectionedAccessedTraceId]) {
        let alert = ViewControllerFactory.Alert.createDataAccessAlertViewController(accesses: accesses, allAccessesPressed: allAccessesPressed)
        alert.modalTransitionStyle = .crossDissolve
        alert.modalPresentationStyle = .overCurrentContext
        (self.tabBarController ?? self).present(alert, animated: true, completion: nil)
    }

    private func allAccessesPressed() {
        let viewController = ViewControllerFactory.Main.createDataAccessViewController()
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DocumentListViewController: UnsafeAddress, LogUtil {}

// MARK: - Delegates

extension DocumentListViewController: DocumentViewDelegate {
    func didTapDelete(for document: Document, on: DocumentView) {
        // Not used
    }

    func didSelect(document: Document) {

        let documentName = name(for: document)
        var documents: [Document] = []
        if document is Vaccination {
            documents = self.groupedDocuments[documentName]?.filter { $0 is Vaccination } ?? []
        } else {
            documents.append(document)
        }

        let viewController = ViewControllerFactory.Document.createDocumentViewControllerTab()
        viewController.delegate = self
        viewController.documents = documents
        viewController.documentName = documentName
        viewController.focusedDocument = document

        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension DocumentListViewController: DocumentViewControllerDelegate {
    func didTabDelete(document: Document, viewController: UIViewController) {
        delete(document: document, viewController: viewController)
    }
}

// MARK: - Accessibility

extension DocumentListViewController {

    private func setupAccessibility() {
        addButton.accessibilityLabel = L10n.Test.Add.title
        UIAccessibility.setFocusTo(navigationbarTitleLabel, notification: .layoutChanged, delay: 0.8)
    }

    private func setupAccessibilityViewOrder() {
        if documents.isEmpty {
            // If no documents
            self.view.accessibilityElements = [navigationbarTitleLabel, dataAccessBanners, subtitleLabel, descriptionLabel, addButton].map { $0 as Any }
        } else {
            // If have documents
            self.view.accessibilityElements = [navigationbarTitleLabel, dataAccessBanners, scrollView, addButton].map { $0 as Any }
        }
    }
}
