import UIKit
import RxSwift
import LucaUIComponents

class NewPrivateMeetingViewController: UIViewController {
	@IBOutlet weak var qrImageView: UIImageView!
	@IBOutlet weak var timerLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var guestsLabel: UILabel!
	@IBOutlet weak var slider: CheckinSliderButton!
	@IBOutlet weak var sliderLabel: UILabel!
	@IBOutlet weak var infoButton: UIButton!
	@IBOutlet weak var timeLengthTitleLabel: Luca16PtLabel!
	@IBOutlet weak var infoButtonImageView: UIImageView!
	@IBOutlet weak var participantsTitleLabel: Luca16PtLabel!

	weak var rootViewController: RootCheckinViewController!
	private var disposeBag: DisposeBag?
	var viewModel: PrivateMeetingViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		setupViews()
		infoButton.isHidden = true
		infoButtonImageView.isHidden = true
	}

	#if DEBUG
	deinit {
		print("deinit NewPrivateMeetingViewController")
	}
	#endif

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.startTimer()
		installObservers()
	}

	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		disposeBag = nil
	}

	private func installObservers() {
		let newDisposeBag = DisposeBag()

		viewModel.guestsDriver.drive { [weak self] (guests: [PrivateMeetingGuest]) in
			self?.guestsLabel.text = "\(guests.filter { $0.isCheckedIn }.count)\\\(guests.count)"
			self?.infoButton.isHidden = guests.isEmpty
			self?.infoButton.isEnabled = !guests.isEmpty
			self?.infoButtonImageView.isHidden = guests.isEmpty
		}.disposed(by: newDisposeBag)

		viewModel.timerDriver.drive { [weak self] (timerString: String) in
			self?.timerLabel.text = timerString
		}.disposed(by: newDisposeBag)

		viewModel.newQRData.drive { [weak self] image in
			self?.qrImageView.image = image
		}
		.disposed(by: newDisposeBag)

		slider.completionBlock = { [weak self] in
			self?.slider.processing = true
			let alert = UIAlertController.yesOrNo(title: L10n.Private.Meeting.End.title,
																						message: L10n.Private.Meeting.End.description,
																						onYes: self?.endMeeting,
																						onNo: {self?.slider.processing = false})
			self?.present(alert, animated: true, completion: nil)
		}
		disposeBag = newDisposeBag
	}

	private func resetCheckInSlider() {
		slider.reset()
		sliderLabel.isHidden = false
		sliderLabel.alpha = 1.0
	}

	func endMeeting() {
		viewModel.endMeeting { [weak self] in
			self?.rootViewController?.goToRoot()
		} failure: { [weak self] error in
			DispatchQueue.main.async {
				if let localizedError = error as? LocalizedTitledError {
					let alert = UIAlertController.infoAlert(title: localizedError.localizedTitle, message: localizedError.localizedDescription)
					self?.present(alert, animated: true, completion: nil)
				}
			}
		}
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let dest = segue.destination as? NewPrivateMeetingDetailsViewController, segue.identifier == "showDetail" {
			dest.viewModel = self.viewModel
		}
	}

	func presentInfoViewController(titleText: String, descriptionText: String) {
		let viewController = ViewControllerFactory.Alert.createInfoViewController(titleText: titleText, descriptionText: descriptionText)
		viewController.modalTransitionStyle = .crossDissolve
		viewController.modalPresentationStyle = .overFullScreen
		present(viewController, animated: true, completion: nil)
	}

	func setupViews() {
		self.title = L10n.Private.Meeting.Info.title.string
		self.participantsTitleLabel.text = L10n.Private.Meeting.Participants.title.string
		self.timeLengthTitleLabel.text = L10n.Private.Meeting.length.string
		navigationItem.hidesBackButton = true
		descriptionLabel.text = L10n.Private.Meeting.description
		sliderLabel.text = L10n.Private.Meeting.Accessibility.endMeeting.string
		//		setupAccessibility()
	}
}

extension NewPrivateMeetingViewController: LogUtil, UnsafeAddress {}

// MARK: - Accessibility
// extension NewPrivateMeetingViewController {
//
//	private func setupAccessibility() {
//		qrImageView.isAccessibilityElement = true
//		lengthStackView?.isAccessibilityElement = true
//		guestsStackView?.isAccessibilityElement = true
//
//		qrImageView.accessibilityLabel = L10n.Contact.Qr.Accessibility.qrCode
//		if let text = timerLabel.text {
//			lengthStackView?.accessibilityLabel = L10n.Private.Meeting.Accessibility.length(text)
//		}
//
//		let guests = self.meeting.guests.filter { $0.isCheckedIn }.count
//		let totalGuests = uniqueGuests.count
//		guestsStackView?.accessibilityLabel = L10n.Private.Meeting.Accessibility.guests(guests, totalGuests)
//
//		self.view.accessibilityElements = [descriptionLabel, qrImageView, lengthStackView, guestsStackView, infoButton, slider.sliderImage].map { $0 as Any}
//	}
// }
