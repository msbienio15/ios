import Foundation
import RxSwift
import RxCocoa

// swiftlint:disable:next type_body_length
final class PrivateMeetingViewModel: LogUtil, UnsafeAddress {

	private var meetingRelay: BehaviorRelay<PrivateMeetingData>
	private(set) var meetingDriver: Driver<PrivateMeetingData>!
	private(set) var guestsAsStringsDriver: Driver<[String]>!
	private(set) var guestsDriver: Driver<[PrivateMeetingGuest]>!

	private var timerRelay = BehaviorRelay<String>(value: "")
	private(set) var timerDriver: Driver<String>!

	private(set) var newQRData: Driver<UIImage?>!

	private var disposeBag = DisposeBag()

	init(meeting: PrivateMeetingData) {
		meetingRelay = BehaviorRelay<PrivateMeetingData>(value: meeting)
		meetingDriver = meetingRelay.asDriver()

		timerDriver = timerRelay.asDriver()

		Observable<Int>.timer(.seconds(1), period: .seconds(10), scheduler: LucaScheduling.backgroundScheduler)
			.flatMapFirst { [weak self] _ -> Single<PrivateMeetingData> in
				guard let selfRef = self else {throw RxError.disposed(object: PrivateMeetingViewModel.self)}
				return ServiceContainer.shared.privateMeetingService.refresh(meeting: selfRef.meetingRelay.value)
			}
			.observe(on: MainScheduler.instance)
			.do(onNext: { [weak self] refreshedMeeting in
				self?.meetingRelay.accept(refreshedMeeting)
			})
			.logError(self, "Meeting fetch")
			.retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
			.subscribe()
			.disposed(by: disposeBag)

		guestsAsStringsDriver = meetingRelay
			.map { $0.guests }
			.map {
				return
					$0.map {
						try? ServiceContainer.shared.privateMeetingService.decrypt(guestData: $0, meetingKeyIndex: meeting.keyIndex)
					}
					.compactMap { $0 }
					.enumerated()
					.map { "\($0+1)  \($1.fn) \($1.ln)" }
			}
			.map {
				Array(Set($0))
			}
			.asDriver(onErrorJustReturn: [""])

		guestsDriver = meetingRelay.map {$0.guests}.asDriver(onErrorJustReturn: [])

		newQRData = Single<UIImage?>.from { [weak self] in
			if let url = try? ServiceContainer.shared.privateMeetingQRCodeBuilderV3.build(scannerId: meeting.ids.scannerId).generatedUrl,
				 let data = url.data(using: .utf8) {
				return self?.setupQrImage(qrCodeData: data)
			}
			return nil
		}.asDriver(onErrorJustReturn: nil)
	}

	func startTimer() {
		CheckinTimer.shared.delegate = self
		CheckinTimer.shared.start(from: meetingRelay.value.createdAt)
	}

	func endMeeting(completion:@escaping (() -> Void), failure:@escaping((Error) -> Void)) {
		ServiceContainer.shared.privateMeetingService.close(meeting: meetingRelay.value) {
			DispatchQueue.main.async {
				CheckinTimer.shared.stop()
				completion()
			}
		} failure: { (error) in
			failure(error)
		}
	}

	private	func setupQrImage(qrCodeData: Data) -> UIImage? {

		// Temp QR Code generation.
		let qrCode = QRCodeGenerator.generateQRCode(data: qrCodeData)
		if let qr = qrCode {
			let transform = CGAffineTransform(scaleX: 10, y: 10)
			let scaledQr = qr.transformed(by: transform)

			return UIImage(ciImage: scaledQr)

		}
		return nil
	}
}

extension PrivateMeetingViewModel: TimerDelegate {
	func timerDidTick() {
		timerRelay.accept(CheckinTimer.shared.counter.formattedTimeString)
	}
}
