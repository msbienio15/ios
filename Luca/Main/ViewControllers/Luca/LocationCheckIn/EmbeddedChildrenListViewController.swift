import UIKit
import RxSwift
import RxCocoa
import LucaUIComponents

class EmbeddedChildrenListViewController: ChildrenListViewController {
	private var currentTraceInfo: TraceInfo?
	@IBOutlet weak var noContentImageView: UIImageView!
	var viewModel: LocationCheckInViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = L10n.Checkin.Kids.checkin
		self.addChildButton.setTitle(L10n.Checkin.Kids.add.uppercased(), for: .normal)
		self.addChildButton.setTitle(L10n.Checkin.Kids.add.uppercased(), for: .highlighted)
	}

	override func updateDescriptionLabel() {
		self.descriptionLabel.text = L10n.Checkin.Kids.description
	}

	override func updateUI() {
		if self.persons.count > 0 {
			self.tableView.isHidden = false
			self.addChildButton.isHidden = true
			self.noContentImageView.isHidden = true
			self.tableView.reloadData()
		} else {
			self.tableView.isHidden = true
			self.addChildButton.isHidden = false
			self.noContentImageView.isHidden = false
		}
	}

	override func reloadData() {
		let currentTraceInfo = ServiceContainer.shared.traceIdService.currentTraceInfo

		let people = ServiceContainer.shared.personService
			.retrieve { _ in
				return true
			}

		Observable.combineLatest(currentTraceInfo.asObservable(), people.asObservable())
			.observe(on: MainScheduler.instance)
			.map({ (traceInfo: TraceInfo, people: [Person]) -> (TraceInfo, [Person]) in
				return (traceInfo, people)
			})
			.do(onNext: { [weak self] (tuple: (TraceInfo, [Person])) in
				self?.currentTraceInfo = tuple.0
				self?.persons = tuple.1
				self?.updateUI()
				self?.updateDescriptionLabel()
			})
			.subscribe()
			.disposed(by: disposeBag)
	}

	private func togglePersonAssociate(_ person: Person, indexPath: IndexPath) {
		if let traceInfo = currentTraceInfo {
			self.viewModel.togglePersonAssociate(person, traceInfo: traceInfo)
				.do(onCompleted: { [weak self] in
					self?.reloadData()
				}, onSubscribe: { [weak self] in
//					if let cell = self?.tableView.cellForRow(at: indexPath) as? EmbeddedChildrenCheckinCell {
//						cell.loading.accept(true)
//					}
				}, onDispose: { [weak self] in
//					if let cell = self?.tableView.cellForRow(at: indexPath) as? EmbeddedChildrenCheckinCell {
//						cell.loading.accept(false)
//					}
				})
				.subscribe()
				.disposed(by: disposeBag)
		}
	}
}

extension EmbeddedChildrenListViewController {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.section == 0 {
			let person = persons[indexPath.row]
			self.togglePersonAssociate(person, indexPath: indexPath)
		} else {
			self.didTapAdd()
		}
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell: UITableViewCell
		if indexPath.section == 0 {
			cell = self.tableView.dequeueReusableCell(withIdentifier: "EmbeddedChildrenCheckinCell", for: indexPath)

			let person = persons[indexPath.row]
			cell.selectionStyle = .none
			cell.backgroundColor = .clear
			if let c = cell as? EmbeddedChildrenCheckinCell {
				c.setup(with: person, traceInfo: self.currentTraceInfo)
			}
		} else {
			cell = self.tableView.dequeueReusableCell(withIdentifier: "EmbeddedAddChildrenCell", for: indexPath)
			if let c = cell as? EmbeddedAddChildrenCell {
				c.setup()
			}
		}

		return cell
	}

	override func numberOfSections(in tableView: UITableView) -> Int {
		return 2
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if section == 0 {
			return persons.count
		} else {
			return 1
		}
	}

	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return indexPath.section == 0
	}
}

class EmbeddedChildrenCheckinCell: UITableViewCell {
	@IBOutlet weak var theTextLabel: Luca14PtBoldLabel!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var checkmarkImageView: UIImageView!
	private var currentTraceInfo: TraceInfo?
	var loading = BehaviorRelay<Bool>(value: false)

	var disposeBag = DisposeBag()

	override func prepareForReuse() {
		disposeBag = DisposeBag()
	}

	func setup(with person: Person, traceInfo: TraceInfo?) {
		self.currentTraceInfo = traceInfo
		theTextLabel.text = person.formattedName
		activityIndicator.isHidden = true
		guard let traceInfo = traceInfo else {
			checkmarkImageView.image = nil
			return
		}
		checkmarkImageView.image = person.isAssociated(with: traceInfo) ? Asset.checkmarkSelected.image : Asset.checkmarkUnselected.image

		self.loading.bind(to: activityIndicator.rx.isAnimating).disposed(by: disposeBag)
		self.loading.bind(to: checkmarkImageView.rx.isHidden).disposed(by: disposeBag)

	}
}

class EmbeddedAddChildrenCell: UITableViewCell {
	@IBOutlet weak var theTextLabel: Luca14PtBoldLabel!

	func setup() {
		self.theTextLabel.text = L10n.Checkin.Kids.addAnother.uppercased()
	}
}
