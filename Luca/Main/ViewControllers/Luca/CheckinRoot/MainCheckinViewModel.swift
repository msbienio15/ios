import UIKit
import RxSwift
import RxCocoa
import RxAppState

class MainCheckinViewModel: NSObject, LogUtil, UnsafeAddress, ViewControllerOwnedVM {
	private(set) weak var owner: UIViewController?
	private let disposeBag = DisposeBag()

	// MARK: - Input
	let mainCheckinViewControllerVisible = BehaviorRelay<Bool>(value: false)

	// MARK: - Output
	var processingSubject = BehaviorSubject<Bool>(value: false)
	var processing: Driver<Bool> {
		processingSubject
			.distinctUntilChanged()
			.asDriver(onErrorJustReturn: false)
	}
	private var displayHUDSubject = PublishSubject<Bool>()
	var displayHUD: Driver<Bool> {
		displayHUDSubject.asDriver(onErrorJustReturn: false)
	}
	var checkin: Driver<TraceInfo>!
	var showScanner: Driver<Bool>!

	init(owner: UIViewController? = nil) {
		super.init()
		self.owner = owner
		self.bindOutput()
	}

	#if DEBUG
	deinit {
		print("deinit MainCheckinViewModel")
	}
	#endif

	func checkIn(selfCheckin: SelfCheckin) -> Single<Bool> {
		return
			Single<SelfCheckin>.just(selfCheckin)
			.do(onSuccess: { _ in
				self.processingSubject.onNext(true)
			})
			.flatMap { (pendingCheckin: SelfCheckin) -> Single<(SelfCheckin, Bool)> in
				// check if this is a private meeting checkin. If so, ask the user if they are sure that they want to do this (since the host gets data).
				return self.checkForPrivateMeeting(pendingCheckin: pendingCheckin)
			}
			.flatMap { pendingCheckin in
				if pendingCheckin.1 == true {
					return
						self.checkInOnServer(selfCheckin: pendingCheckin.0)
						.andThen(self.checkTraceStatus())
						.do(onSubscribed: {
							self.displayHUDSubject.onNext(true)
						}, onDispose: {
							self.displayHUDSubject.onNext(false)
						})
				}
				self.processingSubject.onNext(false)
				return Single.just(false)
			}
			.debug("Self checkin")
			.retry(delay: .seconds(1), scheduler: MainScheduler.instance)
	}

	private func bindOutput() {
		checkin =
			ServiceContainer.shared.traceIdService
			.onCheckInRx()
			.asDriver(onErrorDriveWith: .empty())

		showScanner = Observable.combineLatest(UIApplication.shared.rx.currentAndChangedAppState, self.mainCheckinViewControllerVisible.asObservable())
			.flatMap({ (appState: AppState, visible: Bool) -> Observable<Bool> in
				return .just(appState == .active && visible)
			})
			.distinctUntilChanged()
			.asDriver(onErrorJustReturn: false)

	}

	private func checkTraceStatus() -> Single<Bool> {
		return ServiceContainer.shared.traceIdService.fetchTraceStatusRx()
			.do(onSuccess: { (success: Bool) in  // resume scanner if checkin was not successful
				if !success {
					self.processingSubject.onNext(false)
					//					throw PrintableError(
					//							title: L10n.Checkin.Failure.NotAvailableAnymore.message,
					//							message: L10n.LocationCheckinViewController.CheckOutFailed.LowDuration.message
					//					)
				}
			})
			.catch { error in
				return Single.error(
					PrintableError(
						title: L10n.MainTabBarViewController.ScannerFailure.title,
						message: error.localizedDescription
					)
				)
			}
	}

	private func checkInOnServer(selfCheckin: SelfCheckin) -> Completable {
		ServiceContainer.shared.traceIdService.checkIn(selfCheckin: selfCheckin)
			.catch { error in
				return Completable.error(
					PrintableError(
						title: L10n.MainTabBarViewController.ScannerFailure.title,
						message: error.localizedDescription
					)
				)
			}
	}

	private func checkForPrivateMeeting(pendingCheckin: SelfCheckin) -> Single<(SelfCheckin, Bool)> {

		if let owner = self.owner, let privateCheckin = pendingCheckin as? PrivateMeetingSelfCheckin {
			return UIAlertController.okAndCancelAlertRx(viewController: owner,
																									title: L10n.Navigation.Basic.hint,
																									message: L10n.Private.Meeting.Alert.description)
				.map {(privateCheckin, $0)}
		}
		// not a private meeting checkin
		return Single<(SelfCheckin, Bool)>.just((pendingCheckin, true))
	}

}
