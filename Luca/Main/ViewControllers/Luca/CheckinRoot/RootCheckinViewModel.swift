import UIKit
import RxSwift
import RxCocoa
import RxAppState

enum PollingFrequency: Int {
	case `default` = 10
	case myQRCodePresented = 1
}

class RootCheckinViewModel: NSObject, LogUtil, UnsafeAddress {
	private let disposeBag = DisposeBag()
	private var pollingDisposeBag = DisposeBag()

	// MARK: - Input
	let rootCheckinViewControllerVisible = BehaviorRelay<Bool>(value: false)
	let pollingFrequency = BehaviorRelay<PollingFrequency>(value: .default) // changing the polling frequency will restart the polling loop
	let pollingShouldStart = BehaviorRelay<Bool>(value: false)

	override init() {
		super.init()
		self.bindOutput()
	}

	private func bindOutput() {
		/*
		polling is not started until either a the scanner has successfully scanned a QR code or the MyQRCodeViewController has been opened the first time. In both cases the polling starts (pollingShouldStart => true). When the MyQRCodeViewController is opened the polling frequency is set to 1 seconds. When it is closed it is set to the default (10 seconds).
		When the app gets minimized / closed the pollingShouldStart variable gets set back to false so that we can start with a new session when the app opens the next time.
		*/

		// stop polling when the app is minimized / closed
		UIApplication.shared.rx.currentAndChangedAppState.asDriver(onErrorJustReturn: .inactive).drive { [weak self] state in
			if state != .active {
				self?.stopPolling()
			} else {
				// if we are still in myQRCodePresented frequency, that means we closed the app on the myQRCode screen. So restart the polling
				if self?.pollingFrequency.value == .myQRCodePresented {
					self?.pollingShouldStart.accept(true)
				}
			}
		}.disposed(by: disposeBag)

		let appActiveAndVisible = Observable.combineLatest(UIApplication.shared.rx.currentAndChangedAppState,
																											 self.rootCheckinViewControllerVisible.asObservable()).map { (t: (AppState, Bool)) in
																												return t.0 == .active && t.1
																											}

		Observable.combineLatest(appActiveAndVisible,
														 self.pollingFrequency.asObservable(),
														 self.pollingShouldStart.asObservable())
			.filter({ (_, _, pollingWakeUpSignal: Bool) in
				// when someone sets pollingShouldStart to true we should start polling again
				pollingWakeUpSignal == true
			})
			.flatMap({ (active: Bool, pollingFrequency: PollingFrequency, _:Bool) -> Observable<(Bool, PollingFrequency)> in
				return .just((active, pollingFrequency))
			})
			.distinctUntilChanged({ (t1: (Bool, PollingFrequency), t2: (Bool, PollingFrequency)) in
				t1.0 == t2.0 && t1.1.rawValue == t2.1.rawValue
			})
			.asDriver(onErrorJustReturn: (false, PollingFrequency.default))
			.drive { [weak self] active, pollingFrequency in
				if active {
					self?.startPolling(frequency: pollingFrequency)
				} else {
					self?.pollingDisposeBag = DisposeBag()
				}
			}.disposed(by: disposeBag)
	}

	private func startPolling(frequency: PollingFrequency) {
		self.pollingDisposeBag = DisposeBag()
		Observable<Int>.timer(.seconds(0), period: .seconds(10), scheduler: LucaScheduling.backgroundScheduler)
			.flatMapFirst { _ in ServiceContainer.shared.traceIdService.fetchTraceStatusRx() }
			.debug("Checking poll")
			.logError(self, "Checkin poll")
			.retry(delay: .seconds(1), scheduler: LucaScheduling.backgroundScheduler)
			.subscribe()
			.disposed(by: pollingDisposeBag)
	}

	func stopPolling() {
		self.pollingDisposeBag = DisposeBag()
		self.pollingShouldStart.accept(false)
	}

	func startPolling() {
		self.pollingShouldStart.accept(true)
	}
}
