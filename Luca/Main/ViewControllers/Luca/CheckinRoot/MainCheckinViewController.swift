import Foundation
import UIKit
import LucaUIComponents
import RxSwift
import AVFoundation
import JGProgressHUD
import AVKit

class MainCheckinViewController: UIViewController, BindableType, HandlesLucaErrors {
	@IBOutlet var showQRCodeButton: LightStandardButton!
	@IBOutlet var privateMeetingButton: DarkStandardButton!
	@IBOutlet weak var scanViewDescriptionLabel: UILabel!
	@IBOutlet weak var scanPreviewWrapper: DesignableView!
	@IBOutlet weak var activateScannerView: DesignableView!
	@IBOutlet weak var activateScannerLabel: Luca14PtLabel!
	@IBOutlet weak var activateScannerButton: UIButton!
	@IBOutlet weak var lightButton: UIButton!

	var viewModel: MainCheckinViewModel!
	weak var rootViewModel: RootCheckinViewModel!
	private var checkInDisposeBag: DisposeBag?
	private var onCheckInDisposeBag: DisposeBag?
	private var loadingHUD = JGProgressHUD.lucaLoading()
	private let disposeBag = DisposeBag()

	weak var rootViewController: RootCheckinViewController?

	private lazy var scannerService: ScannerService = {
		let ss = ScannerService()
		return ss
	}()

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = L10n.Checkin.noun
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
		self.activateScannerLabel.text = L10n.Camera.Access.Activate.label.string
		privateMeetingButton.setTitle(L10n.Button.Private.meeting.uppercased(), for: .normal)
		scanPreviewWrapper.accessibilityLabel = L10n.Contact.Qr.Accessibility.qrCodeScanner
		self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: L10n.Navigation.Tab.history.uppercased(), style: .plain, target: self, action: #selector(showHistory))
		guard let device = AVCaptureDevice.default(for: .video) else {
			self.lightButton.isHidden = true
			return
		}
		self.lightButton.isHidden = !device.hasTorch || !device.isTorchAvailable
	}

	#if DEBUG
	deinit {
		print("deinit MainCheckinViewController")
	}
	#endif

	func bindViewModel() {
		//		viewModel.processing
		//			.drive { [weak self] processing in
		//				processing ? self?.scannerService.stopRunning() : self?.scannerService.startRunning()
		//			}
		//			.disposed(by: disposeBag)
		//
		//		viewModel.displayHUD
		//			.drive { [weak self] displayHUD in
		//				if let self = self, displayHUD {
		//						self.loadingHUD.show(in: self.view)
		//				} else {
		//						self?.loadingHUD.dismiss()
		//				}
		//			}
		//			.disposed(by: disposeBag)

		viewModel.checkin
			.debug()
			.drive { [weak self] _ in
				if self?.presentedViewController != nil {
					self?.dismiss(animated: true) {
						self?.showCheckinOrMeetingViewController(animated: true)
//						let viewController = ViewControllerFactory.Checkin.createLocationCheckinViewController(traceInfo: traceInfo)
//						self?.navigationController?.pushViewController(viewController, animated: true)
					}
				} else {
					self?.showCheckinOrMeetingViewController(animated: true)
//					let viewController = ViewControllerFactory.Checkin.createLocationCheckinViewController(traceInfo: traceInfo)
//					self?.navigationController?.pushViewController(viewController, animated: true)
				}
			}
			.disposed(by: disposeBag)

		viewModel.showScanner.drive { [weak self] show in
			self?.showScanner(display: show)
		}.disposed(by: disposeBag)
	}

	private func showScanner(display: Bool) {
		if display == false {
			self.endScanner()
		} else {
			if !scannerService.scannerOn {
				DispatchQueue.main.async {
					if AVCaptureDevice.authorizationStatus(for: .video) == .authorized {
						self.scanPreviewWrapper.isHidden = false
						self.activateScannerView.isHidden = true
						self.startScanner()
					} else {
						self.scanPreviewWrapper.isHidden = true
						self.activateScannerView.isHidden = false
					}
				}
			}
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.mainCheckinViewControllerVisible.accept(true)

		// In the case When checking out and returning back to this view controller, stop the scanner if it is still running
		//		DispatchQueue.main.async(execute: endScanner)

		showCheckinOrMeetingViewController(animated: false)

		installObservers()

		if !remindIfPhoneNumberNotVerified() {
			remindIfAddressNotFilled()
		}

		//		if !scannerService.scannerOn {
		//			DispatchQueue.main.async {
		//				AVCaptureDevice.authorizationStatus(for: .video) == .authorized
		//					? self.startScanner()
		//					: AVCaptureDevice.requestAccess(for: .video, completionHandler: self.didGetAccessResult(canAccessCamera:))
		//			}
		//		} else {
		//			DispatchQueue.main.async(execute: endScanner)
		//		}
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		viewModel.mainCheckinViewControllerVisible.accept(false)
		//		self.endScanner()

		onCheckInDisposeBag = nil
	}

	@objc private func showHistory() {
		let viewController = ViewControllerFactory.History.createHistoryViewController()
		navigationController?.pushViewController(viewController, animated: true)
	}

	private func showPrivateMeetingViewController(meeting: PrivateMeetingData?, animated: Bool) {
		if let meeting = meeting ?? ServiceContainer.shared.privateMeetingService.currentMeeting {
			rootViewController?.showPrivateMeeting(meeting: meeting, animated: animated)
		}
	}

	private func didGetAccessResult(canAccessCamera: Bool) {
        DispatchQueue.main.async {
            if !canAccessCamera {
                self.goToApplicationSettings()
            } else {
                self.scanPreviewWrapper.isHidden = false
                self.activateScannerView.isHidden = true
                self.startScanner()
            }
        }
	}

	private func goToApplicationSettings() {
		UIAlertController(title: L10n.Camera.Access.title, message: L10n.Camera.Access.description, preferredStyle: .alert).goToApplicationSettings(viewController: self, pop: true)
	}

	private func startScanner() {
		scannerService.startScanner(onParent: self, in: scanPreviewWrapper, type: .checkin) { [weak self] in
			// scan was successful
			// TODO: maybe start a timer here to restart the scanner if nothing comes from the checkin-check-loop?
//			self?.viewModel.checkIn(selfCheckin: checkin)
			self?.rootViewModel.startPolling()
		}
	}

	private func endScanner() {
		scannerService.endScanner()
	}

	@IBAction func showMyQRCodePressed(_ sender: UIButton) {
		let viewController = ViewControllerFactory.Checkin.createMyQRViewController(rootViewModel: self.rootViewModel)
		rootViewModel.pollingFrequency.accept(.myQRCodePresented)
		rootViewModel.startPolling()
		navigationController?.present(viewController, animated: true)
	}

	private func showCheckinOrMeetingViewController(animated: Bool) {
		if ServiceContainer.shared.privateMeetingService.currentMeeting != nil {
			showPrivateMeetingViewController(meeting: nil, animated: animated)
			return
		}
		_ = ServiceContainer.shared.traceIdService.currentTraceInfo
			.observe(on: MainScheduler.instance)
			.do(onNext: { traceInfo in
				self.rootViewController?.showActiveCheckinViewStack(traceInfo: traceInfo, animated: animated)
			})
			.subscribe()
	}

	// swiftlint:disable:next function_body_length
	private func installObservers() {

		let disposeBag = DisposeBag()

//		ServiceContainer.shared.traceIdService
//			.onCheckInRx()
//			.asDriver(onErrorDriveWith: .empty())
//			.do(onNext: { _ in
//				self.log("Checked in!")
//				self.endScanner()
//				if self.presentedViewController != nil {
//					self.dismiss(animated: true) { // dismiss the modal QR Code Controller if it's currently presented
//						self.showCheckinOrMeetingViewController(animated: true)
//					}
//				} else {
//					self.showCheckinOrMeetingViewController(animated: true)
//				}
//			})
//			.drive()
//			.disposed(by: disposeBag)

		onCheckInDisposeBag = disposeBag
	}

	/// Returns true if phone number is not verified yet
	private func remindIfPhoneNumberNotVerified() -> Bool {
		if !LucaPreferences.shared.phoneNumberVerified {
			let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.attention, message: L10n.Verification.PhoneNumber.notYetVerified) {
				let viewController = ViewControllerFactory.Main.createContactViewController()
				self.navigationController?.pushViewController(viewController, animated: true)
			}
			present(alert, animated: true, completion: nil)
			return true
		}
		return false
	}

	/// Returns true if data is incomplete
	private func remindIfAddressNotFilled() {
		if !ServiceContainer.shared.userService.isDataComplete {
			let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.attention, message: L10n.UserData.addressNotFilledMessage) {
				let viewController = ViewControllerFactory.Main.createContactViewController()
				self.navigationController?.pushViewController(viewController, animated: true)
			}
			present(alert, animated: true, completion: nil)
		}
	}

	@IBAction func privateMeetingPressed(_ sender: UIButton) {
		UIAlertController(title: L10n.Private.Meeting.Start.title,
											message: L10n.Private.Meeting.Start.description,
											preferredStyle: .alert)
			.actionAndCancelAlert(
                actionText: L10n.Navigation.Basic.start,
                action: createPrivateMeeting,
                viewController: self
            )
	}

	private func createPrivateMeeting() {
		if ServiceContainer.shared.privateMeetingService.currentMeeting == nil {
			_ = ServiceContainer.shared.privateMeetingService.createMeeting()
				.subscribe(on: MainScheduler.instance)
				.do(onSubscribe: { self.loadingHUD.show(in: self.view) })
				.do(onDispose: { self.loadingHUD.dismiss() })
				.do(onSuccess: { meeting in
					self.showPrivateMeetingViewController(meeting: meeting, animated: true)
				})
				.do(onError: { error in
					let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.error, message: L10n.Private.Meeting.Create.failure(error.localizedDescription))
					self.present(alert, animated: true, completion: nil)
				})
				.subscribe()
		}
	}
	@IBAction func activateScannerPressed(_ sender: Any) {
		DispatchQueue.main.async {
			AVCaptureDevice.requestAccess(for: .video, completionHandler: self.didGetAccessResult(canAccessCamera:))
		}
	}
	@IBAction func lightButtonTapped(_ sender: Any) {
		guard let device = AVCaptureDevice.default(for: .video) else { return }
		defer { device.unlockForConfiguration() }
		do {
			try device.lockForConfiguration()
			if device.torchMode == .off {
				try device.setTorchModeOn(level: 1.0)
			} else {
				device.torchMode = .off
			}
		} catch let error {
			//			print(error)
		}
	}

}

extension MainCheckinViewController: UnsafeAddress, LogUtil {}
