import UIKit

class CheckinNavigationController: UINavigationController {}

class ActiveCheckinNavigationController: UINavigationController {
	// fix for bug in UIKit: https://stackoverflow.com/a/28794826/29909
	override var tabBarController: UITabBarController? {
		return nil
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if #available(iOS 13.0, *) {
			UIApplication.shared.setStatusBarStyle(.darkContent, animated: animated)
		} else {
			UIApplication.shared.setStatusBarStyle(.default, animated: animated)
		}
	}

	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		if #available(iOS 13.0, *) {
			UIApplication.shared.setStatusBarStyle(.lightContent, animated: animated)
		} else {
			UIApplication.shared.setStatusBarStyle(.default, animated: animated)
		}
	}
}

class RootCheckinViewController: UIViewController {
	private var currentController: UIViewController?
	private var mainCheckinViewControllerStack: CheckinNavigationController?
	var viewModel: RootCheckinViewModel!

	override func viewWillAppear(_ animated: Bool) {
		viewModel.rootCheckinViewControllerVisible.accept(true)
		super.viewWillAppear(animated)
	}

	override func viewWillDisappear(_ animated: Bool) {
		viewModel.rootCheckinViewControllerVisible.accept(false)
		super.viewWillDisappear(animated)
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let dest = segue.destination as? CheckinNavigationController, segue.identifier == "embedMainCheckinViewController" {
			if var contr = dest.viewControllers.first as? MainCheckinViewController {
				self.currentController = dest
				self.mainCheckinViewControllerStack = dest
				contr.rootViewController = self
				contr.rootViewModel = self.viewModel
				contr.bindViewModel(to: MainCheckinViewModel(owner: contr))
			}
		}
	}

	func showActiveCheckinViewStack(traceInfo: TraceInfo, animated: Bool) {
		let activeCheckinViewController = ViewControllerFactory.Checkin.createLocationCheckinViewController(traceInfo: traceInfo)
		activeCheckinViewController.rootViewController = self
		let navigationController = ActiveCheckinNavigationController(rootViewController: activeCheckinViewController)
		self.viewModel.startPolling()
		self.switchToController(controller: navigationController, duration: animated ? 0.5 : 0.0, switchType: .show)
	}

	func showPrivateMeeting(meeting: PrivateMeetingData, animated: Bool) {
		let viewController = ViewControllerFactory.Checkin.createPrivateMeetingViewController(meeting: meeting)
		viewController.rootViewController = self
		viewController.viewModel = PrivateMeetingViewModel(meeting: meeting)
		let navigationController = ActiveCheckinNavigationController(rootViewController: viewController)
		self.switchToController(controller: navigationController, duration: animated ? 0.5 : 0.0, switchType: .show)
	}

	func goToRoot() {
		if let vc = mainCheckinViewControllerStack {
//			self.viewModel.stopPolling()
			self.switchToController(controller: vc, duration: 0.5, switchType: .dismiss)
		}
	}

	enum ControllerSwitchType {
		case show
		case dismiss
	}

	private func switchToController(controller: UIViewController, duration: TimeInterval, switchType: ControllerSwitchType) {
		controller.view.frame = self.view.bounds

		if let currentController = self.currentController {
			currentController.willMove(toParent: nil)

			if duration <= 0.0 {
				self.view.insertSubview(controller.view, aboveSubview: currentController.view)
				self.addChild(controller)
				currentController.removeFromParent()
				controller.didMove(toParent: self)
				currentController.view.removeFromSuperview()
				self.currentController = controller
				return
			}

			self.addChild(controller)

			if switchType == .show {
				self.view.insertSubview(controller.view, aboveSubview: currentController.view)
				controller.view.transform = CGAffineTransform(translationX: 0,
																											y: currentController.view.frame.size.height)
			} else {
				self.view.insertSubview(controller.view, belowSubview: currentController.view)
			}
			UIView.animate(withDuration: duration, delay: 0.0, options: .curveEaseInOut, animations: {
				if switchType == .show {
					controller.view.transform = CGAffineTransform(translationX: 0, y: 0)
				} else {
					currentController.view.transform = CGAffineTransform(translationX: 0, y: controller.view.frame.size.height)
				}
			}) { (_) in
				currentController.removeFromParent()
				controller.didMove(toParent: self)
				currentController.view.removeFromSuperview()
				self.currentController = controller
			}
		}
		//		else {
		//			self.addChild(controller)
		//			self.view.addSubview(controller.view)
		//			controller.didMove(toParent: self)
		//			self.currentController = controller
		//		}
	}
}
