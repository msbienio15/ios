import UIKit
import AVFoundation
import RxSwift

class QRScannerViewController: UIViewController {

    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

    var type: QRType?

    var onSuccess: (() -> Void)?
		private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
			#if DEBUG
//			var checkedIn = false
//			let stringValue = "https://app-dev.luca-app.de/webapp/9fc9cd8c-1274-49cf-8354-454aae1c4778#eyJ0YWJsZSI6OX0/CWA1/CAESNwgBEhZ1bmRlZmluZWQgUG9tbWVzYnVkZSAxGhtKY" +
//				"Whuc3RyYcOfZSA2LCAxMDk2NyBCZXJsaW4adggBEmCDAszMTXne1DAA5_YxmhRdd_NZN2VKl9L32Jl9-ZybE4b2eNIrhFOKYU4XAOHq3RPLDxdHTW6ANiO24rCOO4rj06HzcVZy3pel58-L1KSPG-_PneL2BoyZQRz3qlu2hoAaEKyw7dnYtnKIYRmBfUhaL4YiBggBEAEYeA"
//			if let url = URL(string: stringValue), checkedIn == false {
//				self.processQRCode(qrCode: stringValue, viewController: self)
//				checkedIn = true
//			}
			#endif

        captureSession = AVCaptureSession()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if previewLayer != nil {
            previewLayer.frame = view.layer.bounds
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationbar()

        DispatchQueue.main.async {
            self.startScanning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopRunning()
    }

    func setupNavigationbar() {
        set(title: L10n.Test.Scanner.title)
    }

    // MARK: - Public interface
    func remove() {
        stopRunning()

        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }

    func present(onParent parent: UIViewController, in parentView: UIView) {
        parent.addChild(self)
        parentView.addSubview(self.view)
        didMove(toParent: parent)

        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: parentView.topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: parentView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: parentView.trailingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: parentView.bottomAnchor).isActive = true
    }

    // MARK: - Private helper
    private func startScanning() {
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            scanningFailed()
            return
        }

        if captureSession.canAddInput(videoInput) {
            captureSession.addInput(videoInput)
        } else {
            scanningFailed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if captureSession.canAddOutput(metadataOutput) {
            captureSession.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            scanningFailed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.frame = view.layer.bounds
        view.layer.addSublayer(previewLayer)
        DispatchQueue.main.async {
            self.captureSession.startRunning()
        }
    }

    func startRunning() {
        self.captureSession.startRunning()
    }

    func stopRunning() {
        if let session = captureSession, session.isRunning {
            self.captureSession.stopRunning()
        }
    }

    private func scanningFailed() {
        let alert = UIAlertController.infoAlert(title: L10n.Navigation.Basic.error, message: L10n.Camera.Error.scanningFailed)
        self.present(alert, animated: true)
        captureSession = nil
    }

//    private func setNavigationBarToTranslucent() {
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.isTranslucent = true
//    }
}
extension QRScannerViewController: AVCaptureMetadataOutputObjectsDelegate {

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            stopRunning()

            _ = ServiceContainer.shared.qrProcessingService.processQRCode(qr: stringValue, presenter: self)
                .do(onError: { error in
                    DispatchQueue.main.async {
                        UIAlertController.presentErrorAlert(presenter: self, for: error, onOk: {
                                                            self.startRunning()
                                                            })
                    }
                }, onCompleted: {
                    guard let success = self.onSuccess else {
                        self.startRunning()
                        return
                    }
                    success()
                }).subscribe()
        }
    }

//	private func processQRCode(qrCode: String, presenter: QRScannerViewController) {
//        ServiceContainer.shared.qrProcessingService.processQRCode(qr: qrCode, presenter: presenter)
//			.do { [weak self] lucaQRCodeFound in
//				if self?.onSuccess == nil || !lucaQRCodeFound {
//						self?.startRunning()
//						return
//				}
//				if let success = self?.onSuccess {
//					success()
//				}
//			}  onError: { [weak self] error in
//				DispatchQueue.main.async {
//						if let titledError = error as? LocalizedTitledError {
//								let alert = UIAlertController.infoAlert(title: titledError.localizedTitle,
//																												message: titledError.localizedDescription,
//																												onOk: {
//																													self?.startRunning()
//																												})
//								self?.present(alert, animated: true, completion: nil)
//						}
//				}
//			}
//			.subscribe()
//			.disposed(by: disposeBag)
//	}
}
