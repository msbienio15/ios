import UIKit
import RxSwift
import JGProgressHUD
import Alamofire

enum TabBarIndex: Int {
    case checkin = 0
    case documents = 1
    case history = 2
    case account = 3
}

class MainTabBarViewController: UITabBarController, HandlesLucaErrors {

    private var disposeBag = DisposeBag()

    private var progressHud = JGProgressHUD.lucaLoading()

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        tabBar.barTintColor = .black
        tabBar.backgroundColor = .black
        tabBar.tintColor = .white
        tabBar.isTranslucent = false

        let borderView = UIView(frame: CGRect(x: 0, y: 0, width: tabBar.frame.size.width, height: 1))
        borderView.backgroundColor = .lucaGrey
        tabBar.addSubview(borderView)

        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let tabBarItems = tabBar.items {
            tabBar.selectionIndicatorImage = UIImage().createTabBarSelectionIndicator(tabSize: CGSize(width: tabBar.frame.width/CGFloat(tabBarItems.count), height: tabBar.frame.height))
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.changeTabBar(to: .documents)

        ServiceContainer.shared.userService.registerIfNeeded { result in
            if result == .userRecreated {
                ServiceContainer.shared.traceIdService.disposeData(clearTraceHistory: true)
            }
            DispatchQueue.main.async { self.progressHud.dismiss() }
        } failure: { (error) in
            DispatchQueue.main.async {
                self.progressHud.dismiss()
                let alert = UIAlertController.infoAlert(title: error.localizedTitle,
                                                        message: error.localizedDescription)
                self.present(alert, animated: true, completion: nil)
            }
        }

        subscribeToSelfCheckin()
        subscribeToDocumentUpdates()

        // Check accessed TraceIDs once in app runtime lifecycle
        ServiceContainer.shared.accessTraceIdChecker
            .fetch()
            .logError(self, "Accessed Trace Id check")
            .subscribe()
            .disposed(by: disposeBag)

        ServiceContainer.shared.documentProcessingService
            .deeplinkStore
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { deepLink in
                if !deepLink.isEmpty {
                    self.parseQRCode(testString: deepLink)
                }
            })
            .disposed(by: disposeBag)

        // If app was terminated
        if ServiceContainer.shared.privateMeetingService.currentMeeting != nil {
            self.changeTabBar(to: .checkin)
        }
        _ = ServiceContainer.shared.traceIdService.isCurrentlyCheckedIn
            .observe(on: MainScheduler.instance)
            .do(onSuccess: { checkedIn in
                if checkedIn { self.changeTabBar(to: .checkin) }
            })
            .subscribe()
            .disposed(by: disposeBag)

        // If entering app from background
        UIApplication.shared.rx.applicationWillEnterForeground
            .flatMap { _ -> Single<(Bool)> in
                ServiceContainer.shared.traceIdService.isCurrentlyCheckedIn.map { checkedIn -> (Bool) in
                    let privateMeeting = ServiceContainer.shared.privateMeetingService.currentMeeting != nil
                    return (checkedIn || privateMeeting)
                }
            }.asObservable()
            .observe(on: MainScheduler.instance)
            .do(onNext: { checkedIn in
                if checkedIn { self.changeTabBar(to: .checkin) }
            }).subscribe()
            .disposed(by: disposeBag)
    }

    private func parseQRCode(testString: String) {
        let alert = ViewControllerFactory.Alert.createTestPrivacyConsent(confirmAction: {
            ServiceContainer.shared.documentProcessingService
                .parseQRCode(qr: testString, additionalValidators: self.initAdditionalValidators())
                .subscribe(onError: { error in
                    DispatchQueue.main.async {
                        UIAlertController.presentErrorAlert(presenter: self, for: error)
                    }
                })
                .disposed(by: self.disposeBag)
        })
        alert.modalTransitionStyle = .crossDissolve
        alert.modalPresentationStyle = .overCurrentContext
        present(alert, animated: true, completion: nil)
    }

    private func initAdditionalValidators() -> [DocumentValidator] {
        [VaccinationBirthDateValidator(presenter: self,
                                       userFirstName: LucaPreferences.shared.firstName ?? "",
                                       userLastName: LucaPreferences.shared.lastName ?? "",
                                       documentSource: ServiceContainer.shared.documentRepoService.currentAndNewTests,
                                       personsSource: ServiceContainer.shared.personRepo.restore())]
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disposeBag = DisposeBag()
    }

    private func subscribeToSelfCheckin() {
        // Continuously check if there is any pending self check in request and consume it if its the case
        ServiceContainer.shared.selfCheckin
            .pendingSelfCheckinRx
            .flatMap { pendingCheckin -> Observable<(SelfCheckin, Bool)> in
                if let privateCheckin = pendingCheckin as? PrivateMeetingSelfCheckin {
                    return UIAlertController.okAndCancelAlertRx(viewController: self, title: L10n.Navigation.Basic.hint, message: L10n.Private.Meeting.Alert.description).map { (privateCheckin, $0) }.asObservable()
                }
                return Observable.of((pendingCheckin, true))
            }
            .filter { $0.1 }
            .flatMap { pendingCheckin in
                return Completable.from { ServiceContainer.shared.selfCheckin.consumeCurrent() }
                    .andThen(ServiceContainer.shared.traceIdService.checkIn(selfCheckin: pendingCheckin.0))
                    .do(onSubscribe: {
                            DispatchQueue.main.async {
                                self.progressHud.show(in: self.view)
                                self.changeTabBar(to: .checkin)
                            }
                    })
                    .andThen(ServiceContainer.shared.traceIdService.fetchTraceStatusRx())
                    .do(onDispose: { DispatchQueue.main.async { self.progressHud.dismiss() } })
            }
            .ignoreElementsAsCompletable()
            .debug("Self checkin")
            .catch {
                self.rxErrorAlert(for: $0)
            }
            .logError(self, "Pending self checkin")
            .retry(delay: .seconds(1), scheduler: MainScheduler.instance)
            .subscribe()
            .disposed(by: disposeBag)

    }

    private func subscribeToDocumentUpdates() {
        ServiceContainer.shared.documentRepoService
            .documentUpdateSignal
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: {
                self.changeTabBar(to: .documents)
            }).disposed(by: disposeBag)
    }

    private func rxErrorAlert(for error: Error) -> Completable {
        self.processErrorMessagesRx(viewController: self, error: error)
            .andThen(Completable.error(error)) // Push the error through to retry the stream
    }

    private func changeTabBar(to index: TabBarIndex) {
        self.selectedIndex = index.rawValue
    }
}

extension MainTabBarViewController: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        return viewController != tabBarController.selectedViewController
    }

}

extension MainTabBarViewController: UnsafeAddress, LogUtil {}
