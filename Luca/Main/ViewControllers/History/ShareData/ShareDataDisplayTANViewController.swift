import UIKit
import LucaUIComponents
import RxCocoa
import RxSwift

class ShareDataDisplayTANViewController: UIViewController, LucaModalAppearance {
	@IBOutlet weak var descriptionLabel: Luca14PtLabel!
	@IBOutlet weak var tanLabel: TANLabel!
	@IBOutlet weak var continueButton: LightStandardButton!
	var TAN: String!

	private let disposeBag = DisposeBag()
	weak var viewModel: ShareDataViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = L10n.History.Share.DisplayTAN.title.string
		self.descriptionLabel.text = L10n.History.Share.DisplayTAN.description.string
		for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.disabled] {
			self.continueButton.setTitle(L10n.History.Share.DisplayTAN.continueButtonTitle.uppercased(), for: state)
		}
		self.tanLabel.text = TAN
		self.tanLabel.accessibilityLabel = TAN.map { String($0) + " " }.joined()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		applyColors()
	}

	@IBAction func finishButtonPressed(_ sender: Any) {
		dismiss(animated: true, completion: nil)
	}
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
