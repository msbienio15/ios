import UIKit
import LucaUIComponents
import RxCocoa
import RxSwift

class ShareDataGenerateTANViewController: UIViewController, BindableType, LucaModalAppearance, HandlesLucaErrors {
	@IBOutlet weak var descriptionLabel: Luca14PtLabel!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!
	@IBOutlet weak var continueButton: LightStandardButton!

	private let disposeBag = DisposeBag()
	weak var viewModel: ShareDataViewModel!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = L10n.History.Share.GenerateTAN.title.string
		self.descriptionLabel.text = L10n.History.Alert.description(viewModel.selectedNumberOfDays.value.row+1, L10n.History.Alert.link)
		for state in [UIControl.State.normal, UIControl.State.highlighted, UIControl.State.disabled] {
			self.continueButton.setTitle(L10n.History.Share.GenerateTAN.continueButtonTitle.uppercased(), for: state)
		}
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		applyColors()
	}

	func bindViewModel() {
		viewModel.generatingTAN.drive { isGenerating in
			self.continueButton.isHidden = isGenerating
			self.activityIndicator.isHidden = !isGenerating
		}.disposed(by: disposeBag)

		viewModel.newTANDriver.drive { [weak self] newTAN in
			self?.performSegue(withIdentifier: "displayTAN", sender: newTAN)
		}.disposed(by: disposeBag)

		self.continueButton.rx.tap.bind(to: viewModel.generateTAN).disposed(by: disposeBag)

		viewModel.errorMessageDriver.drive { [weak self] (error: PrintableError) in
			self?.processErrorMessages(error: error)
		}.disposed(by: disposeBag)

	}

	// MARK: - Navigation

	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destination.
	// Pass the selected object to the new view controller.
		if let TAN = sender as? String,
			 let vc = segue.destination as? ShareDataDisplayTANViewController,
			 segue.identifier == "displayTAN" {
			vc.TAN = TAN
		}
	}

}
