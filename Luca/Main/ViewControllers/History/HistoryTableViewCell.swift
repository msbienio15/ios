import UIKit
import RxSwift
import RxCocoa

enum TableViewPosition {
    case last
    case first
    case only
}

class HistoryTableViewCell: UITableViewCell {
	@IBOutlet weak var checkinLocationNameLabel: UILabel!
	@IBOutlet weak var checkinGroupNameLabel: UILabel!
	@IBOutlet weak var checkinKidsNameLabel: UILabel?
	@IBOutlet weak var checkinDateLabel: UILabel!
	@IBOutlet weak var topHistoryLineView: UIView!
	@IBOutlet weak var bottomHistoryLineView: UIView!
	@IBOutlet weak var disclosureArrowImageView: UIImageView?
    @IBOutlet weak var dotIndicatorView: UIView!

    var infoPressedActionHandler: (() -> Void)?

	override func prepareForReuse() {
		super.prepareForReuse()
		checkinGroupNameLabel.isHidden = false
		checkinKidsNameLabel?.isHidden = true
		disclosureArrowImageView?.isHidden = true
	}

    func setup(historyEvent: HistoryEvent, children: [Person]?, hasAccessedTraceIds: Bool = false, isRead: Bool = true) {
		self.selectionStyle = .none
		self.accessoryType = .none
		checkinGroupNameLabel.isHidden = false
		checkinKidsNameLabel?.isHidden = true
		disclosureArrowImageView?.isHidden = !hasAccessedTraceIds

		if let userEvent = historyEvent as? UserEvent {
			setupUserEvent(userEvent: userEvent, children: children)
		} else if let userUpdate = historyEvent as? UserDataUpdate {
			checkinLocationNameLabel.text = L10n.History.Data.updated
			checkinGroupNameLabel.isHidden = true
			checkinKidsNameLabel?.isHidden = true
			checkinDateLabel.text = userUpdate.date.formattedDateTime
			disclosureArrowImageView?.isHidden = false
		} else if let event = historyEvent as? UserDataTransfer {
			checkinLocationNameLabel.text = L10n.History.Data.shared
			checkinGroupNameLabel.isHidden = true
			checkinKidsNameLabel?.isHidden = true
			checkinDateLabel.text = event.date.formattedDateTime
			disclosureArrowImageView?.isHidden = false
		}

        if isRead {
            dotIndicatorView.backgroundColor = .white
            checkinLocationNameLabel.textColor = .white
        } else {
            dotIndicatorView.backgroundColor = Asset.lucaOrange.color
            checkinLocationNameLabel.textColor = Asset.lucaOrange.color
        }

		bottomHistoryLineView.isHidden = false
		topHistoryLineView.isHidden = false
	}

	func setupUserEvent(userEvent: UserEvent, children: [Person]?) {
		checkinLocationNameLabel.isHidden = false

		if let locationName = userEvent.checkin.location?.locationName {
			checkinLocationNameLabel.text = locationName
			checkinGroupNameLabel.isHidden = userEvent.checkin.location?.groupName != nil
			checkinGroupNameLabel.text = userEvent.checkin.location?.groupName
		} else {
			checkinLocationNameLabel.text = userEvent.checkin.location?.groupName ?? userEvent.checkin.location?.name
			checkinGroupNameLabel.text = nil
			checkinGroupNameLabel.isHidden = true
		}

        if let children = children, children.count > 0 {
            checkinKidsNameLabel?.isHidden = false
            checkinKidsNameLabel?.text = L10n.History.Kids.title(children.map {$0.firstName}.asSentence)
        }

		checkinDateLabel.text = userEvent.formattingCheckinCheckoutDate()

		if let checkout = userEvent.checkout {
			checkinDateLabel.accessibilityLabel = L10n.History.Checkin.Checkout.time(userEvent.checkin.date.accessibilityDate, checkout.date.accessibilityDate)
		} else {
			checkinDateLabel.accessibilityLabel = userEvent.checkin.date.accessibilityDate
		}

		if userEvent.checkin.role == .host {
			checkinGroupNameLabel.isHidden = false
			checkinLocationNameLabel.text = "\(L10n.Private.Meeting.Info.title)"
			if let checkout = userEvent.checkout {
				checkinGroupNameLabel.text = "\(L10n.Private.Meeting.Participants.title): \(checkout.guestlist?.count ?? 0)"
			} else {
				checkinGroupNameLabel.text = "\(L10n.Private.Meeting.Participants.title): \(userEvent.checkin.guestlist?.count ?? 0)"
			}
			disclosureArrowImageView?.isHidden = false
		}
	}

	@IBAction func infoPressed(_ sender: UIButton) {
		self.infoPressedActionHandler?()
	}

	func setupHistoryLineViews(position: TableViewPosition) {
		switch position {
		case .only:
			bottomHistoryLineView.isHidden = true
			topHistoryLineView.isHidden = true
		case .last:
			bottomHistoryLineView.isHidden = true
		case .first:
			topHistoryLineView.isHidden = true
		}
	}

}
