import UIKit
import RxSwift
import RxCocoa

struct HistoryEventViewModel {
    var event: HistoryEvent
    var persons: [Person] = []
    var accessedTraceIds: [AccessedTraceId] = []
}

final class HistoryViewModel: LogUtil, UnsafeAddress {

    // MARK: Bindings

    let isLoadingEvents = BehaviorRelay<Bool>(value: true)
    var data: Observable<[HistoryEventViewModel]> {
        updateSignal
            .flatMapLatest { _ in
                Observable.zip(
                    self.events,
                    self.children,
                    self.accessedTraceIdRepo.restore().asObservable()
                )
            }
            .map { events, children, accessesTraceIds -> [HistoryEventViewModel] in
                events.map { event in
                    if let userEvent = event as? UserEvent {

                        var filteredTraceIds = accessesTraceIds.filter { $0.traceId == userEvent.checkin.traceInfo?.traceId }

                        if let filter = self.filterForWarningLevel {
                            filteredTraceIds = filteredTraceIds.filter { $0.warningLevel == filter }
                        }

                        return HistoryEventViewModel(
                            event: userEvent,
                            persons: children[userEvent.checkin.traceInfo?.traceId ?? ""] ?? [],
                            accessedTraceIds: filteredTraceIds
                        )
                    }
                    return HistoryEventViewModel(event: event)
                }
            }
            .map { viewModels in
                if self.filterForWarningLevel != nil {
                    return viewModels.filter { !$0.accessedTraceIds.isEmpty }
                }
                return viewModels
            }
    }

    /// contains mapping of traceId to children in HistoryEvent
    private var children: Observable<[String: [Person]]> {
        events.flatMap { self.loadChildren(for: $0) }
    }

    private lazy var events: Observable<[HistoryEvent]> = {
        updateSignal.flatMap { _ in self.loadEvents() }
            .share(replay: 1, scope: .whileConnected)
    }()

    private var updateSignal: Observable<Void> {
        Observable.merge(
            Observable.just(Void()),
            historyService.onEventAddedRx.map { _ in Void() },
            historyRepo.onDataChanged,
            accessedTraceIdRepo.onDataChanged
        )
    }

    // MARK: Init

    let personService: PersonService
    let historyService: HistoryService
    let historyRepo: HistoryRepo
    let accessedTraceIdRepo: AccessedTraceIdRepo
    let filterForWarningLevel: UInt8?

    init(personService: PersonService,
         historyService: HistoryService,
         historyRepo: HistoryRepo,
         accessedTraceIdRepo: AccessedTraceIdRepo,
         filterForWarningLevel: UInt8? = nil
    ) {
        self.personService = personService
        self.historyService = historyService
        self.historyRepo = historyRepo
        self.filterForWarningLevel = filterForWarningLevel
        self.accessedTraceIdRepo = accessedTraceIdRepo
    }

    private func loadEvents() -> Single<[HistoryEvent]> {
        historyService
            .removeOldEntries()
            .andThen(historyService.historyEvents)
            .observe(on: MainScheduler.instance)
            .do(onSubscribe: {
                self.isLoadingEvents.accept(true)
            })
            .do(onDispose: {
                self.isLoadingEvents.accept(false)
            })
            .map { $0.reversed() }
    }

    private func loadChildren(for events: [HistoryEvent]) -> Single<[String: [Person]]> {
        Observable.from(events)
            .flatMap { event in
                self.loadChildren(for: event)
            }
            .toArray()
            .map { tupleArray in
                var children: [String: [Person]] = [:]
                _ = tupleArray
                    .filter { _, persons in
                        persons.count > 0
                    }
                    .map { traceId, persons in
                        children[traceId] = persons
                    }
                return children
            }
    }

    private func loadChildren(for event: HistoryEvent) -> Maybe<(String, [Person])> {
        if let userEvent = event as? UserEvent,
           let traceInfo = userEvent.checkout?.traceInfo ?? userEvent.checkin.traceInfo {
            return personService.retrieveAssociated(with: traceInfo)
                .map { (traceInfo.traceId, $0) }
                .asMaybe()
        }
        return Maybe.empty()
    }
}
