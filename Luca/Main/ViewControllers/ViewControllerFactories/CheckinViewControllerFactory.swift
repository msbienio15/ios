import UIKit

class CheckinViewControllerFactory {
	static func createContactQRViewControllerTab() -> UIViewController {
		let contactQRViewController = RootCheckinViewController.instantiate(storyboard: UIStoryboard(name: "CheckinRootViewController", bundle: nil), identifier: "RootCheckinViewController")// .fromStoryboard()
		if let controller = contactQRViewController as? RootCheckinViewController {
			controller.viewModel = RootCheckinViewModel()
		}
		//				let contactQRViewController = ContactQRViewController.fromStoryboard()
		//				if var contr = contactQRViewController as? MainCheckinViewController {
		//					contr.bindViewModel(to: CheckinRootViewModel(owner: contr))
		//				}
		//        let navigationController = CheckinNavigationController(rootViewController: contactQRViewController)
		contactQRViewController.tabBarItem.image = UIImage.init(named: "scanner")
		contactQRViewController.tabBarItem.title = L10n.Navigation.Tab.checkin

		return contactQRViewController
	}

	static func createMyQRViewController(rootViewModel: RootCheckinViewModel) -> UIViewController {
		let myQRCodeVC = MyQRCodeViewController.fromStoryboard()
		if var vc = myQRCodeVC as? MyQRCodeViewController {
			vc.bindViewModel(to: MyQRCodeViewModel())
			vc.rootViewModel = rootViewModel
		}
		let navigationController = UINavigationController(rootViewController: myQRCodeVC)
		return navigationController
	}

	static func createMyQRFullScreenViewController() -> UIViewController {
		let myQRCodeVC = MyQRCodeFullScreenViewController.fromStoryboard()
		return myQRCodeVC
	}

	static func createLocationCheckinViewController(traceInfo: TraceInfo) -> ActiveCheckinViewController {
		//        let viewController: LocationCheckinViewController = LocationCheckinViewController.fromStoryboard()
		let viewController: ActiveCheckinViewController = ActiveCheckinViewController.fromStoryboard()
//		viewController.checkinType = .location

		let serviceContainer = ServiceContainer.shared
        viewController.viewModel = DefaultLocationCheckInViewModel(
            traceInfo: traceInfo,
            traceIdService: serviceContainer.traceIdService,
            timer: CheckinTimer.shared,
            preferences: LucaPreferences.shared,
            autoCheckoutService: serviceContainer.autoCheckoutService,
            notificationService: serviceContainer.notificationService)

		return viewController
	}

	static func createPrivateMeetingViewController(meeting: PrivateMeetingData) -> NewPrivateMeetingViewController {
		let vc: NewPrivateMeetingViewController = NewPrivateMeetingViewController.fromStoryboard()
//		vc.meeting = meeting
		return vc
	}

	static func createQRScannerViewController() -> QRScannerViewController {
		return QRScannerViewController()
	}
}
