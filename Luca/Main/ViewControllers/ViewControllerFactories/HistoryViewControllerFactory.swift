import UIKit

class HistoryViewControllerFactory {
    static func createHistoryViewController(filterForWarningLevel: UInt8? = nil) -> UIViewController {
        let vc: HistoryViewController = HistoryViewController.fromStoryboard()

        vc.viewModel = HistoryViewModel(
            personService: ServiceContainer.shared.personService,
            historyService: ServiceContainer.shared.history,
            historyRepo: ServiceContainer.shared.historyRepo,
            accessedTraceIdRepo: ServiceContainer.shared.accessedTraceIdRepo,
            filterForWarningLevel: filterForWarningLevel
        )

		return vc
	}

	static func createShareDialogViewController() -> UIViewController {
		let timeFrameController = ShareDataSelectTimeframeViewController.instantiate(storyboard: UIStoryboard(name: "ShareDataDialog", bundle: nil), identifier: "ShareDataSelectTimeframeViewController")
		let navigationController = UINavigationController(rootViewController: timeFrameController)
		if var vc = timeFrameController as? ShareDataSelectTimeframeViewController {
			vc.bindViewModel(to: ShareDataViewModel())
		}
		return navigationController
	}

}
