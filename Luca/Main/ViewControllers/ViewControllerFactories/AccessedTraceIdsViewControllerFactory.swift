import UIKit

class AccessedTraceIdsViewControllerFactory {
    static func createDetailViewController(accessedTraceId: AccessedTraceId) -> AccessedTraceIdDetailViewController {
        let vc: AccessedTraceIdDetailViewController = AccessedTraceIdDetailViewController.fromStoryboard()
        vc.viewModel = AccessedTraceIdDetailViewModel(
            accessedTraceId: accessedTraceId,
            notificationConfigSource: ServiceContainer.shared.notificationConfigCachedDataSource,
            accessedTraceIdRepo: ServiceContainer.shared.accessedTraceIdRepo
        )
        return vc
    }

    static func createWarningLevelsViewController(traceInfo: TraceInfo) -> WarningLevelsViewController {
        let vc: WarningLevelsViewController = WarningLevelsViewController.fromStoryboard()
        vc.viewModel = WarningLevelsViewModel(
            traceInfo: traceInfo,
            accessedTraceIdRepo: ServiceContainer.shared.accessedTraceIdRepo,
            notificationConfigSource: ServiceContainer.shared.notificationConfigCachedDataSource
        )
        return vc
    }
}
