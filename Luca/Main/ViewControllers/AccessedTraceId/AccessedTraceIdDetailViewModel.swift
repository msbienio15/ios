import Foundation
import RxSwift
import RxCocoa

final class AccessedTraceIdDetailViewModel {
    private let accessedTraceId: AccessedTraceId
    private let notificationConfigSource: CachedDataSource<NotificationConfig>
    private let accessedRepo: AccessedTraceIdRepo

    init(accessedTraceId: AccessedTraceId,
         notificationConfigSource: CachedDataSource<NotificationConfig>,
         accessedTraceIdRepo: AccessedTraceIdRepo
    ) {
        self.accessedTraceId = accessedTraceId
        self.notificationConfigSource = notificationConfigSource
        self.accessedRepo = accessedTraceIdRepo
    }

    var subtitle: Driver<String> {
        notificationConfigSource.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .map { $0.default.retrieveMessages(for: self.accessedTraceId.warningLevel) }
            .map { $0?.title ?? "" }
            .asDriver(onErrorJustReturn: "Subtitle")
    }

    var body: Driver<String> {
        notificationConfigSource.retrieve()
            .compactMap { $0.first }
            .asObservable()
            .map { $0.default.retrieveMessages(for: self.accessedTraceId.warningLevel) }
            .map { $0?.message ?? "" }
            .asDriver(onErrorJustReturn: "Body")
    }

    func setAsRead() -> Completable {
        if self.accessedTraceId.isRead {
            return Completable.empty()
        }
        return Single.from {
            var temp = self.accessedTraceId
            temp.readDate = Date()
            return temp
        }
        .flatMap { self.accessedRepo.store(object: $0) }
        .asCompletable()
    }
}
