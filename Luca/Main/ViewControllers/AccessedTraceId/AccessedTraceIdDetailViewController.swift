import UIKit
import LucaUIComponents
import RxSwift
import RxCocoa

class AccessedTraceIdDetailViewController: UIViewController, BindableType, DisplaysProgress, HandlesLucaErrors {

    var progressIndicator: UIActivityIndicatorView!
    var viewModel: AccessedTraceIdDetailViewModel!

    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!

    private var disposeBag: DisposeBag?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = L10n.AccessedTraceIdDetailViewController.title
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        subtitleLabel.alpha = 0.0
        bodyLabel.alpha = 0.0
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        bindViewModel()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        disposeBag = nil
    }

    func bindViewModel() {
        let newDisposeBag = DisposeBag()

        viewModel.subtitle
            .do(onNext: { _ in
                UIView.animate(withDuration: 0.3) {
                    self.subtitleLabel.alpha = 1.0
                }
            })
            .drive(subtitleLabel.rx.text)
            .disposed(by: newDisposeBag)

        viewModel.body
            .do(onNext: { _ in
                UIView.animate(withDuration: 0.3) {
                    self.bodyLabel.alpha = 1.0
                }
            })
            .drive(bodyLabel.rx.text)
            .disposed(by: newDisposeBag)

        viewModel.setAsRead().subscribe().disposed(by: newDisposeBag)

        disposeBag = newDisposeBag
    }

}
