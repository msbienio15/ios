import UIKit

class DocumentViewFactory {
    static func createView(for document: Document, isExpanded: Bool, with delegate: DocumentViewDelegate) -> DocumentView? {
        if document is CoronaTest {
            return CoronaTestView.createView(document: document, isExpanded: isExpanded, delegate: delegate)
        } else if document is Appointment {
            return AppointmentView.createView(document: document, isExpanded: isExpanded, delegate: delegate)
        } else if document is Vaccination {
            return CoronaVaccineItemView.createView(document: document, isExpanded: isExpanded, delegate: delegate)
        } else if document is Recovery {
            return CoronaRecoveryView.createView(document: document, isExpanded: isExpanded, delegate: delegate)
        }

        return nil
    }

    static func group(views: [DocumentView]) -> [UIView] {

        var groupedViews: [String: [DocumentView]] = [:]
        var returnViews: [UIView] = []

        for item in views {
            if let item = item as? HorizontalGroupable & DocumentView {
                if groupedViews[item.groupedKey] == nil {
                    groupedViews[item.groupedKey] = []
                }
                // If second vaccination, insert at front of the array in order to display it first.
                if let vaccineView = item as? CoronaVaccineItemView, let vaccination = vaccineView.document, vaccination.doseNumber == 2 {
                    groupedViews[item.groupedKey]?.insert(item, at: 0)
                } else {
                    groupedViews[item.groupedKey]?.append(item)
                }
            } else {
                returnViews.append(item)
            }
        }

        for (_, group) in groupedViews {
            let horizontalItemView: HorizontalDocumentListView = HorizontalDocumentListView(views: group)
            returnViews.append(horizontalItemView)
        }

        return returnViews
    }
}
