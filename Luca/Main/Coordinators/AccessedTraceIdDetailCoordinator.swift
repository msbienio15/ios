import UIKit

class AccessedTraceIdDetailCoordinator: Coordinator {

    private let accessedTraceId: AccessedTraceId
    private let presenter: UIViewController

    public init(presenter: UIViewController, accessedTraceId: AccessedTraceId) {
        self.presenter = presenter
        self.accessedTraceId = accessedTraceId
    }

    public func start() {

        let vc = ViewControllerFactory.AccessedTraceIds.createDetailViewController(accessedTraceId: accessedTraceId)
        presenter.navigationController?.pushViewController(vc, animated: true)
    }
}
