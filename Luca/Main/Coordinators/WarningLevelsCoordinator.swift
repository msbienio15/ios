import UIKit

class WarningLevelsCoordinator: Coordinator {

    private let traceInfo: TraceInfo
    private let presenter: UIViewController

    public init(presenter: UIViewController, traceInfo: TraceInfo) {
        self.presenter = presenter
        self.traceInfo = traceInfo
    }

    public func start() {
        let vc = ViewControllerFactory.AccessedTraceIds.createWarningLevelsViewController(traceInfo: traceInfo)
        presenter.navigationController?.pushViewController(vc, animated: true)
    }
}
