import UIKit

protocol DocumentViewDelegate: AnyObject {
    func didSelect(document: Document)
    func didTapDelete(for document: Document, on: DocumentView)
}

protocol DocumentViewProtocol: AnyObject {
    static func createView(document: Document, isExpanded: Bool, delegate: DocumentViewDelegate?) -> DocumentView?
}

class DocumentView: UIView {
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint?
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint?

    var isExpanded: Bool = false

    weak var delegate: DocumentViewDelegate?
    var position: HorizontalDocumentListViewItemPosition = .middle {
        didSet {
            updatePosition()
        }
    }

    func updatePosition() {
        if position == .leading {
            leadingConstraint?.constant = isExpanded ? 8 : 32
            trailingConstraint?.constant = 0
        } else if position == .trailing {
            leadingConstraint?.constant = 0
            trailingConstraint?.constant = isExpanded ? -8 : -32
        } else if position == .single {
            leadingConstraint?.constant = isExpanded ? 8 : 32
            trailingConstraint?.constant = isExpanded ? -8 : -32
        } else if position == .middle {
            leadingConstraint?.constant = 0
            trailingConstraint?.constant = 0
        }
    }
}
