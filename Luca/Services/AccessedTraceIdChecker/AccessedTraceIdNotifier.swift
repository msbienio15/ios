import Foundation
import RxSwift
import BackgroundTasks

class AccessedTraceIdNotifier {

    private let accessedTraceIdChecker: AccessedTraceIdChecker
    private let notificationScheduler: NotificationScheduler
    private let notificationConfigSource: CachedDataSource<NotificationConfig>

    private var disposeBag: DisposeBag?
    private var notificationDisposeBag: DisposeBag?

    var isEnabled: Bool { disposeBag != nil }

    init(accessedTraceIdChecker: AccessedTraceIdChecker,
         notificationScheduler: NotificationScheduler,
         notificationConfigSource: CachedDataSource<NotificationConfig>
    ) {
        self.accessedTraceIdChecker = accessedTraceIdChecker
        self.notificationScheduler = notificationScheduler
        self.notificationConfigSource = notificationConfigSource
    }

    func enable() {
        if isEnabled {
            return
        }
        let newDisposeBag = DisposeBag()
        accessedTraceIdChecker.onNewData
            .observe(on: MainScheduler.instance)
            .flatMap { newNotifications in
                self.notificationConfigSource.retrieve()
                    .compactMap { $0.first }
                    .asObservable()
                    .take(1)
                    .do(onNext: { config in
                        let warningLevels = Set(newNotifications.map { $0.warningLevel })
                        for warningLevel in warningLevels {
                            if let msg = config.default.retrieveMessages(for: warningLevel),
                               let title = msg.title,
                               let banner = msg.banner {

                                self.notificationScheduler.scheduleNotification(title: title, message: banner)
                            }
                        }
                    })
            }
            .subscribe()
            .disposed(by: newDisposeBag)

        self.disposeBag = newDisposeBag
    }

    func disable() {
        disposeBag = nil
    }

    @available(iOS 13.0, *)
    func sendNotificationOnMatch(task: BGAppRefreshTask) {
        let newDisposeBag = DisposeBag()
        accessedTraceIdChecker.fetch()
            .do(onSuccess: { _ in
                task.setTaskCompleted(success: true)
            }, onError: { _ in
                task.setTaskCompleted(success: false)
            })
            .subscribe()
            .disposed(by: newDisposeBag)
        notificationDisposeBag = newDisposeBag
    }

    func sendNotificationOnMatch(completionHandler: @escaping(UIBackgroundFetchResult) -> Void) {
        let newDisposeBag = DisposeBag()
        accessedTraceIdChecker.fetch()
            .do(onSuccess: { data in
                if data.isEmpty {
                    completionHandler(.noData)
                } else {
                    completionHandler(.newData)
                }
            }, onError: { _ in
                completionHandler(.noData)
            })
            .subscribe()
            .disposed(by: newDisposeBag)
        notificationDisposeBag = newDisposeBag
    }

    func disposeNotificationOnMatch() {
        notificationDisposeBag = nil
    }
}
