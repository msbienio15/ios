import Foundation
import RxSwift

class AccessDataChunkHandler {
    private let backendAccessData: BackendAccessDataV4
    private let accessDataSource: CachedDataSource<AccessedTracesDataChunk>
    private let keyValueRepo: KeyValueRepoProtocol
    private let cacheKey = "AccessDataChunkCache"
    typealias DataCache = [String: Data]

    init(backendAccessData: BackendAccessDataV4, accessDataSource: CachedDataSource<AccessedTracesDataChunk>, keyValueRepo: KeyValueRepoProtocol) {
        self.backendAccessData = backendAccessData
        self.accessDataSource = accessDataSource
        self.keyValueRepo = keyValueRepo
    }

    func fetch() -> Single<[AccessedTracesDataChunk]> {
        accessDataSource.retrieve()
            .asObservable()
            .compactMap { $0.first }
            .flatMap { activeChunk -> Observable<AccessedTracesDataChunk> in
                Observable.merge(
                    Observable.just(activeChunk),
                    self.fetchRecurrency(startWith: activeChunk.previousHash.base64EncodedString())
                )
            }
            .toArray()
    }

    private func fetchRecurrency(startWith chunkId: String) -> Observable<AccessedTracesDataChunk> {
        getOrFetchChunk(withId: chunkId)
            .asObservable()
            .catch { error in

                // If it's notFound, it reached the end of the buffer. Otherwise its a legit error which shouldn't be suppressed.
                if let backendError = error as? BackendError<FetchAccessedTracesArchivedChunkError>,
                   backendError.backendError == .notFound {
                    return Observable.empty()
                }
                throw error
            }
            .flatMap { chunk -> Observable<AccessedTracesDataChunk> in
                Observable.merge(Observable.just(chunk), self.fetchRecurrency(startWith: chunk.previousHash.base64EncodedString()))
            }
    }

    private func getOrFetchChunk(withId chunkId: String) -> Single<AccessedTracesDataChunk> {
        getChunkFromCache(withId: chunkId).ifEmpty(switchTo: fetchChunk(withId: chunkId))
    }

    private func getChunkFromCache(withId chunkId: String) -> Maybe<AccessedTracesDataChunk> {
        keyValueRepo.load(cacheKey, type: DataCache.self)
            .catch { _ in Single.just([:]) }
            .asObservable()
            .flatMap { cache -> Observable<AccessedTracesDataChunk> in
                if let found = cache[chunkId],
                   let chunk = try? AccessedTracesDataChunk(data: found) {
                    return Observable.just(chunk)
                }
                return Observable.empty()
            }
            .asMaybe()
    }

    private func fetchChunk(withId chunkId: String) -> Single<AccessedTracesDataChunk> {
        return backendAccessData.archivedChunk(chunkId: chunkId).asSingle()
            .flatMap { chunk in
                self.saveToCache(chunk: chunk, id: chunkId).andThen(Single.just(chunk))
            }
    }

    private func saveToCache(chunk: AccessedTracesDataChunk, id: String) -> Completable {
        keyValueRepo.load(self.cacheKey, type: DataCache.self)
            .catch { _ in Single.just([:]) }
            .flatMapCompletable { cache in
                var mutableCache = cache
                mutableCache[id] = chunk.originalData
                return self.keyValueRepo.store(self.cacheKey, value: mutableCache)
            }
    }
}

extension AccessDataChunkHandler: UnsafeAddress, LogUtil {}
