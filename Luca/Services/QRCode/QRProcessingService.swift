import Foundation
import RxSwift
import JGProgressHUD

class QRProcessingService {

    private let documentProcessingService: DocumentProcessingService
    private let documentRepoService: DocumentRepoService
    private let traceIdService: TraceIdService
    private let qrParser: QRParser
    private let personRepo: PersonRepo
    private let preferences: LucaPreferences

    init(documentProcessingService: DocumentProcessingService,
         documentRepoService: DocumentRepoService,
         documentFactory: DocumentFactory,
         traceIdService: TraceIdService,
         personRepo: PersonRepo,
         preferences: LucaPreferences) {
        self.documentProcessingService = documentProcessingService
        self.documentRepoService = documentRepoService
        self.qrParser = QRParser(documentFactory: documentFactory)
        self.traceIdService = traceIdService
        self.personRepo = personRepo
        self.preferences = preferences
    }

    public func processQRCode(qr: String, presenter: QRScannerViewController) -> Maybe<Bool> {
        qrParser.processQRType(qr: qr)
            .flatMapMaybe { self.showAlertIfWrongType(viewController: presenter, type: $0) }
            .asObservable()
            .flatMap { type -> Maybe<Bool> in
                switch type {
                case .checkin: return self.checkin(qr: qr)
                case .document: return self.processDocument(url: qr, presenter: presenter)
                case .url: return self.openURL(qr: qr)
                }
            }.asMaybe()
    }

    private func checkin(qr: String) -> Maybe<Bool> {
        Single<SelfCheckin>.from {
            guard let url = URL(string: qr), let checkin = CheckInURLParser.parse(url: url) else {
                throw QRProcessingError.parsingFailed
            }
            return checkin
        }.flatMapCompletable { self.traceIdService.checkIn(selfCheckin: $0) }
        .andThen(self.traceIdService.fetchTraceStatusRx())
        .asMaybe()
        .flatMap { _ in Maybe.just(true) }
    }

    private func processDocument(url: String, presenter: UIViewController) -> Maybe<Bool> {
        showTestPrivacyConsent(viewController: presenter)
            .asObservable()
            .flatMap { _ in self.documentProcessingService.parseQRCode(qr: url, additionalValidators: self.initAdditionalValidators(presenter: presenter)).andThen(Maybe.just(true)) }
            .asMaybe()
    }

    private func initAdditionalValidators(presenter: UIViewController) -> [DocumentValidator] {
        [VaccinationBirthDateValidator(
            presenter: presenter,
            userFirstName: preferences.firstName ?? "",
            userLastName: preferences.lastName ?? "",
            documentSource: documentRepoService.currentAndNewTests,
            personsSource: personRepo.restore())]
    }

    private func openURL(qr: String) -> Maybe<Bool> {
        Completable.from {
            guard let url = URL(string: qr) else {
                throw QRProcessingError.parsingFailed
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }.andThen(Maybe.just(false))
        .subscribe(on: MainScheduler.instance)
    }

    // MARK: Alerts

    func showAlertIfWrongType(viewController: QRScannerViewController, type: QRType) -> Maybe<QRType> {
        return Maybe.create { observer -> Disposable in
            let showAlert = viewController.type != type && type != .url
            let title = viewController.type == .checkin ? L10n.Camera.Warning.Checkin.title : L10n.Camera.Warning.Document.title
            let message = viewController.type == .checkin ? L10n.Camera.Warning.Checkin.description : L10n.Camera.Warning.Document.description
            let alert = UIAlertController
                .actionAndCancelAlert(title: title, message: message, actionTitle: L10n.Navigation.Basic.continue, action: {
                    observer(.success(type))
                }, cancelAction: {
                    observer(.completed)
                })
            showAlert ? viewController.present(alert, animated: true, completion: nil) : observer(.success(type))

            return Disposables.create { alert.dismiss(animated: true, completion: nil) }
        }.subscribe(on: MainScheduler.instance)
    }

    func showTestPrivacyConsent(viewController: UIViewController) -> Maybe<Void> {
        return Maybe.create { observer -> Disposable in
            let alert = ViewControllerFactory.Alert.createTestPrivacyConsent(confirmAction: {
                observer(.success(Void()))
            }, cancelAction: {
                observer(.completed)
            })
            alert.modalTransitionStyle = .crossDissolve
            alert.modalPresentationStyle = .overCurrentContext
            viewController.present(alert, animated: true, completion: nil)

            return Disposables.create { alert.dismiss(animated: true, completion: nil) }
        }.subscribe(on: MainScheduler.instance)
    }

}
