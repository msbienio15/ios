import Foundation

class BackendAddressV4: BackendAddressV3 {
    init() {
        super.init(version: "v4")
    }
}

class BackendAccessDataV4 {
    private let backendAddress: BackendAddressV4

    init(backendAddress: BackendAddressV4) {
        self.backendAddress = backendAddress
    }

    func activeChunk() -> AsyncDataOperation<BackendError<FetchAccessedTracesErrorV4>, AccessedTracesDataChunk> {
        FetchAccessedTracesV4AsyncDataOperation(backendAddress: backendAddress)
    }
    func archivedChunk(chunkId: String) -> AsyncDataOperation<BackendError<FetchAccessedTracesArchivedChunkError>, AccessedTracesDataChunk> {
        FetchAccessedTracesArchivedChunkAsyncDataOperation(backendAddress: backendAddress, chunkId: chunkId)
    }
    func fetchNotificationConfig() -> AsyncDataOperation<BackendError<FetchNotificationConfigError>, NotificationConfig> {
        FetchNotificationConfigBinaryAsyncOperation(backendAddress: backendAddress)
    }
}
