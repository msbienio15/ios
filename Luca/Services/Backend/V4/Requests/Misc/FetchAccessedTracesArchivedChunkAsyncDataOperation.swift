import Foundation

enum FetchAccessedTracesArchivedChunkError {
    case notFound
}

extension FetchAccessedTracesArchivedChunkError: RequestError {
    var localizedTitle: String {
        L10n.Navigation.Basic.error
    }

    var errorDescription: String? {
        switch self {
        case .notFound:
            return "\(self)" // This won't be localized as this error is not supposed to be printed for the user. notFound in this case means that there is no more data to download.
        }
    }
}

class FetchAccessedTracesArchivedChunkAsyncDataOperation: BackendBinaryAsyncDataOperation<KeyValueParameters, AccessedTracesDataChunk, FetchAccessedTracesArchivedChunkError> {

    private let requestedChunkId: String

    init(backendAddress: BackendAddressV4, chunkId: String) {
        let fullUrl = backendAddress.apiUrl
            .appendingPathComponent("notifications")
            .appendingPathComponent("traces")
            .appendingPathComponent(chunkId)

        requestedChunkId = chunkId

        super.init(url: fullUrl,
                   method: .get,
                   errorMappings: [404: .notFound])
    }

    override func map(data: Data) throws -> AccessedTracesDataChunk {
        try AccessedTracesDataChunk(data: data)
    }
}
