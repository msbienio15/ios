import UIKit

enum DGCVaccinationType: String, Codable {

    case cormirnaty = "EU/1/20/1528"
    case janssen = "EU/1/20/1525"
    case moderna = "EU/1/20/1507"
    case vaxzevria = "EU/1/21/1529"
    case sputnikV = "Sputnik-V"

    var category: String {
        switch self {
        case .cormirnaty: return L10n.Vaccine.Result.cormirnaty
        case .janssen: return L10n.Vaccine.Result.janssen
        case .moderna: return L10n.Vaccine.Result.moderna
        case .vaxzevria: return L10n.Vaccine.Result.vaxzevria
        case .sputnikV: return L10n.Vaccine.Result.sputnikV
        }
    }
}

struct DGCVaccination: Vaccination {

    var firstName: String
    var lastName: String
    var dateOfBirth: Date
    var vaccineType: String
    var doseNumber: Int
    var dosesTotalNumber: Int
    var date: Date
    var originalCode: String
    var hashSeed: String { originalCode }
    var issuer: String
    var laboratory: String { issuer }
    var vaccinationEntry: DGCVaccinationEntry

    init(cert: DGCCert, vaccine: DGCVaccinationEntry, originalCode: String) {
        self.firstName = cert.firstName
        self.lastName = cert.lastName
        self.dateOfBirth = cert.dateOfBirth
        self.date = vaccine.date
        self.vaccineType = DGCVaccinationType(rawValue: vaccine.medicalProduct)!.category
        self.doseNumber = vaccine.doseNumber
        self.dosesTotalNumber = vaccine.dosesTotal
        self.issuer = vaccine.issuer
        self.vaccinationEntry = vaccine
        self.originalCode = originalCode
    }

    func belongsToUser(withFirstName firstName: String, lastName: String) -> Bool {
        let uppercaseAppFullname = formatUser(withFirstName: firstName, lastName: lastName)
        let uppercaseTestFullname = formatUser(withFirstName: self.firstName, lastName: self.lastName)
        return uppercaseAppFullname == uppercaseTestFullname
    }

    private func formatUser(withFirstName firstName: String, lastName: String) -> String {
        return (firstName + lastName).uppercased()
            .removeOccurences(of: ["DR.", "PROF."])
            .removeNonUppercase()
            .removeWhitespaces()
    }
}

extension DGCVaccination {
    var contentDescription: String {
        let contactData = createContactData()
        let description = createDescription()

        return contactData
        + "\n"
        + L10n.DataReport.Document.Certificate.description(
            L10n.DataReport.Document.CertificateType.dgcVaccination,
            description,
            originalCode,
            L10n.DataReport.Document.Certificate.storagePeriodVaccine)
    }

    private func createContactData() -> String {
        L10n.DataReport.Document.ContactData.dateOfBirth(dateOfBirth.dateString)
    }

    private func createDescription() -> String {
        L10n.DataReport.Document.DescriptionDetails.dgcVaccination(
            vaccinationEntry.diseaseTargeted,
            vaccinationEntry.vaccineOrProphylaxis,
            vaccineType,
            vaccinationEntry.manufacturer,
            vaccinationEntry.doseNumber,
            vaccinationEntry.dosesTotal,
            vaccinationEntry.date.dateString,
            vaccinationEntry.countryCode,
            vaccinationEntry.issuer,
            vaccinationEntry.uvci
        )
    }
}
