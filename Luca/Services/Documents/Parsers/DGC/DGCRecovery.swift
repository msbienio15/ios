import UIKit

struct DGCRecovery: Recovery {
    var firstName: String
    var lastName: String
    var dateOfBirth: Date
    var validFromDate: Date
    var validUntilDate: Date
    var originalCode: String
    var hashSeed: String { originalCode }
    var issuer: String
    var laboratory: String { issuer }
    var recoveryEntry: DGCRecoveryEntry

    init(cert: DGCCert, recovery: DGCRecoveryEntry, originalCode: String) {
        self.firstName = cert.firstName
        self.lastName = cert.lastName
        self.dateOfBirth = cert.dateOfBirth
        self.validFromDate = recovery.validFrom
        self.validUntilDate = recovery.validUntil
        self.issuer = recovery.issuer
        self.recoveryEntry = recovery
        self.originalCode = originalCode
    }

    func belongsToUser(withFirstName firstName: String, lastName: String) -> Bool {
        let uppercaseAppFullname = (firstName + lastName).uppercased()
        let uppercaseTestFullname = (self.firstName + self.lastName).uppercased()
        return uppercaseAppFullname == uppercaseTestFullname
    }
}

extension DGCRecovery {
    var contentDescription: String {
        let contactData = createContactData()
        let description = createDescription()

        return contactData
            + "\n"
            + L10n.DataReport.Document.Certificate.description(
                L10n.DataReport.Document.CertificateType.dgcRecovery,
                description,
                originalCode,
                L10n.DataReport.Document.Certificate.storagePeriodAppointment)
    }

    private func createContactData() -> String {
        L10n.DataReport.Document.ContactData.dateOfBirth(dateOfBirth.dateString)
    }

    private func createDescription() -> String {
        L10n.DataReport.Document.DescriptionDetails.dgcRecovery(
            recoveryEntry.diseaseTargeted,
            recoveryEntry.firstPositiveDate,
            recoveryEntry.countryCode,
            issuer,
            validFromDate.dateString,
            validUntilDate.dateString,
            recoveryEntry.uvci
        )
    }
}
