import Foundation
import RxSwift

class TraceInfoReporter: ContentReporter {

    private let historyService: HistoryService
    private let traceInfoRepo: TraceInfoRepo

    init(historyService: HistoryService, traceInfoRepo: TraceInfoRepo) {
        self.historyService = historyService
        self.traceInfoRepo = traceInfoRepo
    }

    func createReport() -> Single<String> {
        findHistoryTraceIds()
            .flatMap { ids in
                self.traceInfoRepo.restore()
                    .asObservable()
                    .flatMap { Observable.from($0) }
                    .filter { !ids.contains($0.traceId) }
                    .map { $0.contentDescription }
                    .toArray()
            }
            .map { $0.joined(separator: "\n-\n") }
    }

    private func findHistoryTraceIds() -> Single<[String]> {
        self.historyService.historyEvents
            .asObservable()
            .flatMap { Observable.from($0) }
            .compactMap { event in
                if let userEvent = event as? UserEvent,
                   let traceId = userEvent.checkin.traceInfo?.traceId {
                    return traceId
                }
                return nil
            }
            .toArray()
    }
}
