import Foundation
import RxSwift

class HistoryReporter: ContentReporter {

    private let historyService: HistoryService

    init(historyService: HistoryService) {
        self.historyService = historyService
    }

    func createReport() -> Single<String> {
        historyService.historyEvents
            .asObservable()
            .flatMap { Observable.from($0) }
            .map { $0.contentDescription }
            .toArray()
            .map { $0.joined(separator: "\n-\n") }
    }
}
