import Foundation
import RxSwift

class UserDataReporter: ContentReporter {

    private let userService: UserService

    init(userService: UserService) {
        self.userService = userService
    }

    func createReport() -> Single<String> {
        Single.from { self.userService.userDescription() }
    }
}
