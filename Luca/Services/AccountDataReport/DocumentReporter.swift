import Foundation
import RxSwift

class DocumentReporter: ContentReporter {

    private let documentRepoService: DocumentRepoService
    private let preferences: LucaPreferences
    private let personRepo: PersonRepo

    init(documentRepoService: DocumentRepoService, preferences: LucaPreferences, personRepo: PersonRepo) {
        self.documentRepoService = documentRepoService
        self.preferences = preferences
        self.personRepo = personRepo
    }

    func createReport() -> Single<String> {
        documentRepoService
            .currentAndNewTests
            .take(1)
            .flatMap { Observable.from($0) }
            .flatMap { document in
                self.documentOwner(document)
                    .map { $0 + "\n" + document.contentDescription }
                    .ifEmpty(default: document.contentDescription)
            }
            .toArray()
            .map { descriptions in
                descriptions.joined(separator: "\n-\n")
            }
    }

    private func documentOwner(_ document: Document) -> Maybe<String> {
        Maybe.from { document as? AssociableToIdentity }
            .flatMap { associableToIdentity in
                self.retrieveIdentities()
                    .map { identities in
                        identities.first(where: { associableToIdentity.belongsToUser(withFirstName: $0.firstName, lastName: $0.lastName) })
                    }
                    .asMaybe()
                    .unwrapOptional()
                    .map { L10n.DataReport.Document.ContactData.general($0.firstName, $0.lastName) }

            }
    }

    private func retrieveIdentities() -> Single<[(firstName: String, lastName: String)]> {
        personRepo.restore()
            .map { persons in persons.map { (firstName: $0.firstName, lastName: $0.lastName) } }
            .map { $0 + [(firstName: self.preferences.firstName ?? "", lastName: self.preferences.lastName ?? "")] }
    }
}
