import Foundation
import RxSwift

enum CacheValidity {
    case none
    case until(unit: Calendar.Component, count: Int)
    case onceInRuntime
}

protocol DataSource {
    associatedtype T
    func retrieve() -> Single<[T]>
}

protocol CacheDataRepo {
    associatedtype T
    func store(data: [T]) -> Completable
    func retrieve() -> Single<[T]>
}

class CachedDataSource<T> {
    func retrieve(loadOnlyFromCache: Bool = false) -> Single<[T]> {
        fatalError("Not implemented")
    }
}

class BaseCachedDataSource<T, DataSourceType, CacheDataRepoType>: CachedDataSource<T>
            where DataSourceType: DataSource, DataSourceType.T == T,
                  CacheDataRepoType: CacheDataRepo, CacheDataRepoType.T == T {
    private let keyValueRepo: KeyValueRepoProtocol
    private let dataSource: DataSourceType
    private let cache: CacheDataRepoType
    private let cacheValidity: CacheValidity
    private var hasBeenReceived = false

    private var cacheTimestampKey: String = ""

    init(
        keyValueRepo: KeyValueRepoProtocol,
        dataSource: DataSourceType,
        cacheDataRepo: CacheDataRepoType,
        cacheValidity: CacheValidity,
        uniqueCacheIdentifier: String) {
        self.keyValueRepo = keyValueRepo
        self.dataSource = dataSource
        self.cache = cacheDataRepo
        self.cacheValidity = cacheValidity

        super.init()

        cacheTimestampKey = "\(uniqueCacheIdentifier).cacheTimestamp"
    }

    override func retrieve(loadOnlyFromCache: Bool = false) -> Single<[T]> {
        if loadOnlyFromCache {
            return cache.retrieve()
        }
        return cacheTimestamp()
            .ifEmpty(default: Date(timeIntervalSince1970: 0.0))
            .flatMap { timestamp in
                if self.isCacheTimestampValid(date: timestamp) {
                    return self.cache.retrieve()
                }
                return self.retrieveFromSource()
            }
    }

    private func cacheTimestamp() -> Maybe<Date> {
        keyValueRepo.load(cacheTimestampKey, type: Date.self).asObservable().onErrorComplete().asMaybe()
    }

    private func retrieveFromSource() -> Single<[T]> {
        dataSource.retrieve()
            .flatMap { data in
                self.cache.store(data: data)
                    .andThen(self.keyValueRepo.store(self.cacheTimestampKey, value: Date()))
                    .andThen(Completable.from { self.hasBeenReceived = true })
                    .andThen(Single.just(data))
            }
    }

    private func isCacheTimestampValid(date: Date) -> Bool {
        switch cacheValidity {
        case .none:
            return false
        case .onceInRuntime:
            return hasBeenReceived
        case .until(let unit, let count):
            guard let threshold = Calendar.current.date(byAdding: unit, value: -count, to: Date()) else { return false }
            return date > threshold
        }
    }
}

extension DataRepo: CacheDataRepo {
    typealias T = Model
    func retrieve() -> Single<[T]> {
        self.restore()
    }
    func store(data: [Model]) -> Completable {
        self.store(objects: data).asCompletable()
    }
}

extension AsyncDataOperation: DataSource {
    typealias T = Result

    func retrieve() -> Single<[T]> {
        self.asSingle().map { [$0] }
    }
}

class KeyValueRepoCacheWrapper<T>: CacheDataRepo where T: Codable {

    private let keyValueRepo: KeyValueRepoProtocol
    private var key: String = ""

    init(keyValueRepo: KeyValueRepoProtocol, uniqueCacheKey: String) {
        self.keyValueRepo = keyValueRepo
        key = uniqueCacheKey + ".cachedData"
    }

    func store(data: [T]) -> Completable {
        keyValueRepo.store(key, value: data)
    }

    func retrieve() -> Single<[T]> {
        keyValueRepo.load(key, type: [T].self)
    }
}
